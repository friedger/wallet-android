/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gnunet_taler_wallet.ecc;

import android.support.test.runner.AndroidJUnit4;

import com.sun.jna.Native;

import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.ECCSignaturePurpose;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EcDSAPrivateKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EcDSASignature;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNetLibrary;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.nio.ByteBuffer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Unit tests for EcDSA signatures
 *
 * @author Oliver Broome
 */
@RunWith(AndroidJUnit4.class)
public class EcDSASignatureTest {
    private EcDSAPrivateKey privateKey = new EcDSAPrivateKey();
    private ECCSignaturePurpose signaturePurpose = prepareTestPurpose();

    public ECCSignaturePurpose prepareTestPurpose() {
        return new ECCSignaturePurpose(
                prepareBuffer(),
                GNUNetLibrary.
                        GNUNET_SIGNATURE_Purpose.GNUNET_SIGNATURE_PURPOSE_TEST);
    }

    private ByteBuffer prepareBuffer() {
        String testString = "If you want to get anything done in this country, " +
                "you've got to complain till you're blue in the mouth.";

        byte[] stringBytes = Native.toByteArray(testString);
        ByteBuffer stringBuffer = ByteBuffer.allocateDirect(stringBytes.length);
        stringBuffer.put(stringBytes);

        return stringBuffer;
    }

    @Test
    public void testInitialisation() {
        try (EcDSASignature signature = new EcDSASignature(signaturePurpose, privateKey);) {
            assertNotNull("Purpose structure is empty!", signature.getPurpose());
            assertNotNull("Signature structure is empty!", signature.getSignatureStruct());

            assertEquals("Purpose structure does not equal original purpose! (purpose code)",
                    Integer.reverseBytes(signature.getPurpose().purpose),
                    signaturePurpose.getPurposeCode());
            assertEquals("Purpose structure does not equal original purpose! (size)",
                    Integer.reverseBytes(signature.getPurpose().size),
                    signaturePurpose.getSize());
        }
    }
}
