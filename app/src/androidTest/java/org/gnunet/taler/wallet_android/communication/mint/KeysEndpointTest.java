/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.communication.mint;

import android.support.test.runner.AndroidJUnit4;

import org.gnunet.taler.wallet_android.communication.CommunicationTestingConstants;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.net.URL;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Tests the implementation of the /keys endpoint of the REST API
 *
 * @author Oliver Broome
 */
@RunWith(AndroidJUnit4.class)
public class KeysEndpointTest {
    private KeysEndpoint endpoint;

    @After
    public void tearDown() {
        endpoint.close();
    }

    @Before
    public void setUp() throws IOException {
        URL testURL = new URL(CommunicationTestingConstants.TEST_MINT);
        endpoint = new KeysEndpoint(testURL);
        endpoint.connect();
    }

    @Test
    public void testRetrieval() {
        assertFalse("Did not receive any denominations!", endpoint.getDenominations().isEmpty());
        assertFalse("Did not receive any signing keys!", endpoint.getSigningKeys().isEmpty());
    }

    @Test
    public void testVerification() {
        assertTrue("The verification method returned false!", endpoint.verify());
    }
}
