/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gnunet_taler_wallet.utilities;

import android.support.test.runner.AndroidJUnit4;

import org.gnunet.taler.wallet_android.nativeLibs.CryptoTestingConstants;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.utilities.Hash;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Unit tests for the Hash helper class
 *
 * @author Oliver Broome
 */
@RunWith(AndroidJUnit4.class)
public class HashTest implements CryptoTestingConstants {
    private Hash hash;

    @After
    public void tearDown() {
        if (hash != null) {
            hash.close();
        }
    }

    @Test
    public void testHashCreation() {
        hash = new Hash(prebuiltMessage);

        assertNotNull("The Hash object is null!", hash);
    }

    @Test
    public void testHashComparison() {
        try (Hash hash = new Hash(prebuiltMessage)) {
            try (Hash hashTwo = new Hash("Pining for the fjords!?")) {
                assertTrue("The original hash wasn't reported as equal to itself!", hash.equals(hash));
                assertFalse("The two different messages were reported as equal!", hash.equals(hashTwo));
            }
        }
    }

    @Test
    public void testGetNativeSize() {
        hash = new Hash(prebuiltMessage);

        assertTrue(hash.getNativeSize().intValue() > 0);
    }

    @Test
    public void testEncoding() {
        hash = new Hash(prebuiltMessage);

        String encodedHash = hash.getAsciiEncoded();

        assertNotNull(encodedHash);
        assertFalse("".equals(encodedHash));
    }

    @Test
    public void testDestruction() {
        hash = new Hash(prebuiltMessage);

        hash.close();

        assertNull("Hash wasn't freed correctly!", hash.getHashStruct());
    }
}
