/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gnunet_taler_wallet.rsa;

import android.support.test.runner.AndroidJUnit4;

import org.gnunet.taler.wallet_android.nativeLibs.CryptoTestingConstants;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa.RSABlindedMessage;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa.RSABlindingKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa.RSAPrivateKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa.RSASignature;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.utilities.Hash;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.math.BigInteger;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Integration tests for RSA blinding
 *
 * @author Oliver Broome
 */
@RunWith(AndroidJUnit4.class)
public class RSABlindingIT implements CryptoTestingConstants {
    private RSABlindingKey randomBlindingKey;
    private RSAPrivateKey alternativePrivateKey = new RSAPrivateKey(
            "(key-data\n" +
                    " (public-key\n" +
                    "  (rsa\n" +
                    "   (n " +
                    "#00C9A3E160526EB3E25423121BFBC14B1F72636B1F7987FBDF015198F08056107CD35" +
                    "17A4E2A644A08B621044D23964632CA2549874E0BCF20A95233307EF41740C02A2FF" +
                    "C235D7779F52B948170FC3A64D75D7B665A69E93C6BE46A406C0DCEFA2C33D2ECA64" +
                    "589A063CB8F4EDF3FAFA4727A0DAC395BB9B181F06EDD6B240C329A36EB0A119F2F0" +
                    "5AB28FAC73D75F3D6BDF58D0FDC63D33828D5CB7731DAC4F22C0822A9DCFCE34E3E0" +
                    "BAD85FF39C0510DEEB6F202CABAE351F2768612D8C880B349C33E578BFAFA85DC33B" +
                    "DBB24B0CF4EBDA41E8A81638B1FC37DDBECD39BEF758259BA73CEF378B6F8DDF5F6D" +
                    "66B20E5EBF567BE52C392AC6B8ACA5E1BC293#)\n" +
                    "   (e #010001#)\n" +
                    "   )\n" +
                    "  )\n" +
                    " (private-key\n" +
                    "  (rsa\n" +
                    "   (n " +
                    "#00C9A3E160526EB3E25423121BFBC14B1F72636B1F7987FBDF015198F08056107CD35" +
                    "17A4E2A644A08B621044D23964632CA2549874E0BCF20A95233307EF41740C02A2FF" +
                    "C235D7779F52B948170FC3A64D75D7B665A69E93C6BE46A406C0DCEFA2C33D2ECA64" +
                    "589A063CB8F4EDF3FAFA4727A0DAC395BB9B181F06EDD6B240C329A36EB0A119F2F0" +
                    "5AB28FAC73D75F3D6BDF58D0FDC63D33828D5CB7731DAC4F22C0822A9DCFCE34E3E0" +
                    "BAD85FF39C0510DEEB6F202CABAE351F2768612D8C880B349C33E578BFAFA85DC33B" +
                    "DBB24B0CF4EBDA41E8A81638B1FC37DDBECD39BEF758259BA73CEF378B6F8DDF5F6D" +
                    "66B20E5EBF567BE52C392AC6B8ACA5E1BC293#)\n" +
                    "   (e #010001#)\n" +
                    "   (d " +
                    "#0BE69C3AAF22D1831E2C23B97B166174ADAFA876E7195B8841EFAAF326DFED813087B" +
                    "28C427C40D25ACD800F7D763516724FD9131F4C1B21765CDFA0911D9EF1B765C6073" +
                    "F9F189A1E1A405D93698F98BC0D4C673216E62926C508705CF043B30840D6C618DA2" +
                    "9D149928E16BD9E68A3700E3C185A1A4BCF6A37E29336EB9DBC94016A84EAB2C0DF6" +
                    "93AD03E76461D9153EE46CC03E452421E10184F32ADDC7DE0D0112F24E77FE4968F5" +
                    "EE9AFE8A343A32CB4FBF90374FD259996E578485999C2C0772124BD2DC89FD90A0DD" +
                    "A3D8B8097EEDC0752A67BE719D12845E593E02E538E5FBA2EFE8C8826134AA6FFE6C" +
                    "5A6B1BE1CED651F2C0E2C9C08F2A8F6F621#)\n" +
                    "   (p " +
                    "#00DD493A3B0D6FF1A6883EBB91266BA1452FC8B86DC8652D3D88BE19FF666BC68297D" +
                    "483022883A5C0F3BDEEEDBA12A844803D90D92ABFE1EB5044E83F0218636BD388B40" +
                    "00E84B6BCCD165226F5045E37864357EF50DE7CE51CFE846D487366B6D4FB6B05930" +
                    "E3C592197607E385F3542EB02952B3842E01211AC067CA6F282E7#)\n" +
                    "   (q " +
                    "#00E945AE4589A229E7125AA98D05878DA440A2316764B78E85536FF54988629B2D4EF" +
                    "A0B30B33D5B761889367348F84A54AA42BE4682D5C48B4B17F269018A875B8909BCC" +
                    "DC7F2B7046E459F7A6D7ABF420BC1C7683B1AEC1A6187270789852792EB66AE9A7D2" +
                    "9D8D4C15D34C742B77046DB16FFA3E66377AA63896853F14BB975#)\n" +
                    "   (u " +
                    "#7BE093B69F0E2871C618074DF574A09185043E9FF21AEA4B320EECB55671FAAC6BB70" +
                    "4132A1A7C28BB49B693E788BFAD6F41532EAFDC8E9753CA0E9E167B06A5583AFB5FD" +
                    "BDE4A140690D654389BEE7D3EB7F887D0088889650D68D7B6CBB2181005779B9B684" +
                    "9641DF7010C4D8C3DB94D1C466AFCB4CC4DE074437223FB5BA9#)\n" +
                    "   )\n" +
                    "  )\n" +
                    " )\n");

    @After
    public void tearDown() {
        if (randomBlindingKey != null) {
            randomBlindingKey.close();
        }
    }

    @Test
    public void testCreatedBlindingKeysAreDifferent() {
        randomBlindingKey = new RSABlindingKey(2048);

        BigInteger encodedKey = randomBlindingKey.encode();
        randomBlindingKey.close();
        randomBlindingKey = null;

        assertNotNull(encodedKey);

        randomBlindingKey = new RSABlindingKey(2048);

        BigInteger newEncodedKey = randomBlindingKey.encode();
        randomBlindingKey.close();
        randomBlindingKey = null;

        assertNotNull(newEncodedKey);

        assertFalse(encodedKey.equals(newEncodedKey));
    }

    /**
     * ensure that subsequent en- and decoding don't modify the initial private key data
     */
    @Test
    public void testPrivateKeyIntegrity() {
        randomBlindingKey = new RSABlindingKey(1024);

        BigInteger encodedKey = randomBlindingKey.encode();
        randomBlindingKey.close();

        assertNotNull("The encoded key is null!", encodedKey);
        assertTrue("The encoded key is empty!", encodedKey.bitLength() > 0);

        RSABlindingKey rsaBlindingKey = new RSABlindingKey(encodedKey);
        BigInteger newEncodedKey = rsaBlindingKey.encode();

        assertNotNull("The encoded key is null!", newEncodedKey);

        rsaBlindingKey.close();

        assertTrue(encodedKey.equals(newEncodedKey));
    }

    @Test
    public void testBlinding() {
        RSAPrivateKey privateKey = new RSAPrivateKey(prebuiltKey);
        Hash hashedMessage = new Hash(prebuiltMessage);

        RSABlindingKey blindingKey = new RSABlindingKey(1024);

        RSABlindedMessage blindedMessage = new RSABlindedMessage(
                hashedMessage,
                blindingKey,
                privateKey.getPublicKey());

        String blindedHashAscii = null;
        blindedHashAscii = blindedMessage.encode();

        assertNotNull(blindedHashAscii);
        assertFalse(blindedHashAscii.equals(prebuiltMessageHashBase32));

        RSASignature blindedSignature = new RSASignature(blindedMessage, privateKey);
        RSASignature unblindedSignature = new RSASignature(blindedSignature, blindingKey, privateKey.getPublicKey());

        String blindedSignatureEncoded, unblindedSignatureEncoded;
        blindedSignatureEncoded = blindedSignature.encode();
        unblindedSignatureEncoded = unblindedSignature.encode();

        assertNotNull(blindedSignatureEncoded);
        assertNotNull(unblindedSignatureEncoded);
        assertFalse(blindedSignatureEncoded.equals(unblindedSignatureEncoded));
        assertTrue(unblindedSignature.verify(hashedMessage, privateKey.getPublicKey()));
    }
}
