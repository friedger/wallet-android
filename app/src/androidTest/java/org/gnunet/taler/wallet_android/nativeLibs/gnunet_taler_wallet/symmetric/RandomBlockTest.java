/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gnunet_taler_wallet.symmetric;

import android.support.test.runner.AndroidJUnit4;

import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.symmetric.RandomBlock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Unit tests for the random block generation wrapper
 *
 * @author Oliver Broome
 */
@RunWith(AndroidJUnit4.class)
public class RandomBlockTest {
    private RandomBlock randomBlock;

    @Before
    public void setUp() {
        randomBlock = new RandomBlock(RandomBlock.KEY_BLOCK);
    }

    @After
    public void tearDown() {
        randomBlock = null;
    }

    @Test
    public void testInitialisation() {
        assertNotNull("Pointer to random block data is null!", randomBlock.getPointer());
        assertTrue("Data size is invalid!", randomBlock.getNativeSize().intValue() == RandomBlock.KEY_BLOCK);
    }

    @Test
    public void testValues() {
        try (RandomBlock otherRandomBlock = new RandomBlock(RandomBlock.KEY_BLOCK)) {
            assertFalse("Key derivation is returning identical blocks!", Arrays.equals(
                    randomBlock.getPointer().getByteArray(0L, RandomBlock.KEY_BLOCK),
                    otherRandomBlock.getPointer().getByteArray(0L, RandomBlock.KEY_BLOCK)
            ));
        }
    }
}
