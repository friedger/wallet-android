/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.tasks.mint;

import android.content.Context;
import android.support.test.InstrumentationRegistry;

import org.gnunet.taler.wallet_android.communication.CommunicationTestingConstants;
import org.gnunet.taler.wallet_android.database.DatabaseFactory;
import org.gnunet.taler.wallet_android.database.model.infrastructure.MintDescriptor;
import org.gnunet.taler.wallet_android.database.storage.MintsDatabase;
import org.gnunet.taler.wallet_android.ui.fragments.MintAddWorkerFragment;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutionException;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Tests for the mint adding service
 *
 * @author Oliver Broome
 */
public class MintAddTaskTest {
    private long createdMint = -1;
    private Context context;

    @Before
    public void setUp() {
        context = InstrumentationRegistry.getInstrumentation().getTargetContext();
    }

    @After
    public void tearDown() {
        if (createdMint != -1) {
            MintsDatabase database = DatabaseFactory.getMintsDatabase(InstrumentationRegistry.getInstrumentation().getTargetContext());
            database.deleteMint(createdMint);
        }
    }

    @Test
    public void testAddMint() throws MalformedURLException, ExecutionException, InterruptedException {
        MintAddWorkerFragment mockFragment = mock(MintAddWorkerFragment.class);
        when(mockFragment.getContext()).thenReturn(context);

        MintAddTask task = new MintAddTask(new WeakReference<>(mockFragment));

        MintDescriptor testMint = new MintDescriptor(1,
                "Localhost testing mint 2",
                new URL(CommunicationTestingConstants.TEST_MINT),
                null);

        createdMint = task.execute(testMint).get()[0];

        assertTrue("A mint was not created!", createdMint > -1);
    }
}