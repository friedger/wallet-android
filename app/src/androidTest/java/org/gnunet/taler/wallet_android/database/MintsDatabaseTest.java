/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.database;

import android.Manifest;
import android.content.Context;
import android.support.annotation.RequiresPermission;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.gnunet.taler.wallet_android.communication.CommunicationTestingConstants;
import org.gnunet.taler.wallet_android.database.storage.MintsDatabase;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSAPrivateKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSAPublicKey;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Unit tests for the mint table access class
 *
 * @author Oliver Broome
 */
@RunWith(AndroidJUnit4.class)
public class MintsDatabaseTest {
    private static Context context;
    private MintsDatabase database;

    @BeforeClass
    public static void initialise() {
        //the following *should* delegate calls to a separate database, but it seems I'm still missing something here.
        //make sure you aren't storing actual coins in the database if you're running this...

        //context = new RenamingDelegatingContext(InstrumentationRegistry.getTargetContext(), "test.");

        context = InstrumentationRegistry.getTargetContext();
    }

    @AfterClass
    public static void cleanUp() {
        //context.deleteDatabase(DatabaseFactory.DB_NAME);  //It seems a little ham-handed to delete the database afterwards...
    }

    @Before
    public void setUp() {
        database = DatabaseFactory.getMintsDatabase(context);
    }

    @After
    public void tearDown() {
        database.clearAllMints();
        database.close();
    }

    @Test
    public void testOpen() {
        try (MintsDatabase.Reader reader = database.readerFor(database.getAllMints(1))) {
            reader.getNext();
        }
    }

    @Test
    @RequiresPermission(Manifest.permission.INTERNET)
    public void testAddMint() {
        URL mintUrl = null;
        try {
            mintUrl = new URL(CommunicationTestingConstants.TEST_MINT);
        } catch (MalformedURLException e) {
            fail("Bad test URL!");
        }

        long rowId;
        try (EdDSAPrivateKey privateKey = new EdDSAPrivateKey();
             EdDSAPublicKey publicKey = new EdDSAPublicKey(privateKey)) {
            rowId = database.addMint("TestMint", mintUrl, publicKey);

            assertTrue("An invalid row ID was returned!", rowId > -1);

            byte[] originalKey = publicKey.getStruct().q_y;
            byte[] retrievedKey = database.getMintById(rowId).getPublicKey().getStruct().q_y;

            assertTrue("The mint's public key is malformed!",
                    Arrays.equals(originalKey, retrievedKey));
        }

    }
}
