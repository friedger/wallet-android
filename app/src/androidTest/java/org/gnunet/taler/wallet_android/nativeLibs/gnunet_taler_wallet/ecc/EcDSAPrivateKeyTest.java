/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gnunet_taler_wallet.ecc;

import android.support.test.runner.AndroidJUnit4;

import org.gnunet.taler.wallet_android.nativeLibs.CryptoTestingConstants;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EcDSAPrivateKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EcDSAPublicKey;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertNotNull;

/**
 * Integration test for EcDSA functions
 *
 * @author Oliver Broome
 */
@RunWith(AndroidJUnit4.class)
public class EcDSAPrivateKeyTest implements CryptoTestingConstants {
    EcDSAPrivateKey privateKey;

    @Before
    public void setUp() {
        privateKey = new EcDSAPrivateKey();
    }

    @After
    public void tearDown() {
        if (privateKey != null) {
            privateKey.close();
            privateKey = null;
        }
    }

    @Test
    public void testPrivateKeyCreationAndDestruction() {
        assertNotNull("The created key object is null!", privateKey);
        assertNotNull("The created key's structure is null!", privateKey.getStruct());
    }

    @Test
    public void testPublicKeyDerivation() {
        try (EcDSAPublicKey publicKey = privateKey.getPublicKey()) {
            assertNotNull("The generated public key is null!", publicKey);
            assertNotNull("The generated public key structure is null!", publicKey.getStruct());
        }
    }

    @Test
    public void testSubkeyDerivation() {
        try (EcDSAPrivateKey derivedPrivateKey = new EcDSAPrivateKey(privateKey, "test", "testContext")) {
            assertNotNull("The derived key object is null!", derivedPrivateKey);
            assertNotNull("The derived key's structure is null!", derivedPrivateKey.getStruct());
            assertNotNull("The derived key's structure is null!", derivedPrivateKey.getPublicKey());
        }
    }
}
