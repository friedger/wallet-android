/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gnunet_taler_wallet.rsa;

import android.support.test.runner.AndroidJUnit4;

import org.gnunet.taler.wallet_android.nativeLibs.CryptoTestingConstants;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa.RSAPrivateKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa.RSAPublicKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa.RSASignature;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.utilities.Hash;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Tests the RSA helper classes for correct behaviour
 *
 * @author Oliver Broome
 */
@RunWith(AndroidJUnit4.class)
public class RSANativeIT implements CryptoTestingConstants {
    RSAPrivateKey randomPrivateKey,
            constantPrivateKey = new RSAPrivateKey(prebuiltKey);
    private RSASignature signature;

    @BeforeClass
    public static void setupEnvironment() {
    }

    @After
    public void tearDown() {
        if (randomPrivateKey != null) {
            randomPrivateKey.close();
            randomPrivateKey = null;
        }

        if (signature != null) {
            signature.close();
            signature = null;
        }
    }

    /**
     * ensure that a new (different) key is created when the old one is destroyed
     */
    @Test
    public void testCreatedKeysAreDifferent() {
        randomPrivateKey = new RSAPrivateKey(1024);
        String encodedKey = null;

        encodedKey = randomPrivateKey.encode();

        randomPrivateKey.close();

        randomPrivateKey = new RSAPrivateKey(1024);
        String newEncodedKey = randomPrivateKey.encode();

        randomPrivateKey.close();

        assertFalse(encodedKey.equals(newEncodedKey));
    }

    /**
     * ensure that subsequent en- and decoding don't modify the initial private key data
     */
    @Test
    public void testPrivateKeyIntegrity() {
        randomPrivateKey = new RSAPrivateKey(1024);
        String encodedKey = randomPrivateKey.encode();
        randomPrivateKey.close();

        try (RSAPrivateKey newPrivateKey = new RSAPrivateKey(encodedKey)) {
            String newEncodedKey = newPrivateKey.encode();
            assertTrue(encodedKey.equals(newEncodedKey));
        }
    }

    /**
     * ensure that subsequent en- and decoding don't modify the initial public key data
     */
    @Test
    public void testPublicKeyIntegrity() {
        randomPrivateKey = new RSAPrivateKey(1024);
        try (RSAPublicKey originalPublicKey = randomPrivateKey.getPublicKey()) {
            String encodedKey = originalPublicKey.encode();
            try (RSAPublicKey newPublicKey = new RSAPublicKey(encodedKey, false)) {
                String newEncodedKey = newPublicKey.encode();
                assertTrue(encodedKey.equals(newEncodedKey));
            }
        }
    }

    @Test
    public void testSignatureCreation() {
        try (Hash messageHash = new Hash(prebuiltMessage);
             RSASignature signature = new RSASignature(messageHash, constantPrivateKey)) {

            String encodedSignature = signature.encode();

            assertTrue(prebuiltMessageSignature.equals(encodedSignature));
        }
    }

    @Test
    public void testSignatureVerification() {
        try (Hash messageHash = new Hash(prebuiltMessage)) {
            signature = new RSASignature(messageHash, constantPrivateKey);

            String messageHashEncoded = messageHash.getAsciiEncoded();
            assertTrue(prebuiltMessageHashBase32.equals(messageHashEncoded));

            final String encodedSignature = signature.encode();
            assertTrue(encodedSignature.equals(prebuiltMessageSignature));

            boolean verificationResult;

            verificationResult = signature.verify(messageHash, constantPrivateKey.getPublicKey());

            assertTrue(verificationResult);
        }
    }
}
