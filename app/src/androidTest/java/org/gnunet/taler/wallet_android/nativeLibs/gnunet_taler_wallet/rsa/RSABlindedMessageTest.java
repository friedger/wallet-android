/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gnunet_taler_wallet.rsa;

import android.support.test.runner.AndroidJUnit4;

import org.gnunet.taler.wallet_android.nativeLibs.CryptoTestingConstants;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa.RSABlindedMessage;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa.RSABlindingKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa.RSAPrivateKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.utilities.Hash;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Unit tests for blinded RSA signatures
 *
 * @author Oliver Broome
 */
@RunWith(AndroidJUnit4.class)
public class RSABlindedMessageTest {
    private RSABlindedMessage blindedMessage;

    @After
    public void tearDown() {
        if (blindedMessage != null) {
            blindedMessage.close();
            blindedMessage = null;
        }
    }

    @Before
    public void setUp() {
        String message = "Look, I took the liberty of examining that parrot, and the only " +
                "reason it had been sitting on that perch in the first place, was that it had been " +
                "nailed there.";
        Hash hashedMessage = new Hash(message);
        RSABlindingKey blindingKey = new RSABlindingKey(2048);
        RSAPrivateKey privateKey = new RSAPrivateKey(CryptoTestingConstants.prebuiltKey);

        blindedMessage = new RSABlindedMessage(hashedMessage, blindingKey, privateKey.getPublicKey());

        hashedMessage.close();
        blindingKey.close();
        privateKey.close();
    }

    @Test
    public void testInitWithHash() {
        assertNotNull(blindedMessage);
        assertNotNull(blindedMessage.encode());
        assertNotNull(blindedMessage.getPointer());
        assertNotNull(blindedMessage.getNativeSize());
    }

    @Test
    public void testInitWithPointer() {
        fail("not implemented!");
    }

    @Test
    public void testGetNativeSize() {
        assertTrue("getNativeSize() returned an invalid size!", blindedMessage.getNativeSize().longValue() > 0);
        //assertEquals("getNativeSize() returned an invalid size!", XXXXX); //TODO: calculate expected size.
    }

    @Test
    public void testGetPointer() {
        assertNotNull(blindedMessage.getPointer().dump(0, 1));
        //TODO: check that there isn't a null byte at the beginning?
    }

    @Test
    public void testEncode() {
        assertFalse("".equals(blindedMessage.encode()));
        //TODO: get a properly blinded message to compare to.
    }

    @Test(expected = RuntimeException.class)
    public void testDestroy() {
        blindedMessage.close();
        blindedMessage.encode();
    }
}
