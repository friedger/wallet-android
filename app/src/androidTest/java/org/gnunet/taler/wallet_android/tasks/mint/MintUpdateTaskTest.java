/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.tasks.mint;

import android.support.test.InstrumentationRegistry;

import org.gnunet.taler.wallet_android.ui.MintListActivity;
import org.junit.Before;
import org.junit.Test;

import java.lang.ref.WeakReference;
import java.util.concurrent.ExecutionException;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Tests for the mint update service
 *
 * @author Oliver Broome
 */
public class MintUpdateTaskTest {
    private MintUpdateTask service;

    @Before
    public void setUp() {
        MintListActivity mockActivity = mock(MintListActivity.class);
        when(mockActivity.getApplicationContext()).thenReturn(InstrumentationRegistry.getTargetContext());
        service = new MintUpdateTask(new WeakReference<>(mockActivity));
    }

    @Test
    public void testUpdateMints() throws ExecutionException, InterruptedException {
        Boolean[] results = service.execute().get();

        for (Boolean report : results) {
            assertTrue("A mint could not be updated!", report);
        }
    }
}