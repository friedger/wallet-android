/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gcrypt;

import com.ochafik.lang.jnaerator.runtime.NativeSize;
import com.sun.jna.Memory;
import com.sun.jna.Native;
import com.sun.jna.NativeLong;
import com.sun.jna.Pointer;

import org.gnunet.taler.wallet_android.nativeLibs.gcrypt.jna.GcryptLibrary;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

/**
 * Wrapper for a call to libgcrypt's KDF (using scrypt)
 *
 * @author Oliver Broome
 */
public class ScryptKeyDerivation {
    public static Memory derive(String passphrase,
                                Pointer salt,
                                NativeSize saltLength,
                                NativeLong iterations,
                                NativeSize keySize) {
        byte[] passphraseBytes = Native.toByteArray(passphrase, StandardCharsets.US_ASCII.toString());
        int passphraseLength = passphraseBytes.length;

        Buffer passphraseBuffer = ByteBuffer.allocateDirect(passphraseLength);
        Pointer passphrasePointer = Native.getDirectBufferPointer(passphraseBuffer);

        Memory result = new Memory(keySize.longValue()); //this *could* go wrong if we use values larger than the maximum signed long, but then Memory throws an exception.

        GcryptLibrary.gcry_kdf_derive(
                passphrasePointer,
                new NativeSize(passphraseLength),
                GcryptLibrary.gcry_kdf_algos.GCRY_KDF_SCRYPT,
                GcryptLibrary.gcry_md_algos.GCRY_MD_SHA512,
                salt,
                saltLength,
                iterations,
                keySize,
                result);

        return result;
    }
}
