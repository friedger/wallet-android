/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.utilities.JNAhelpers;

import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.PointerUtils;
import com.sun.jna.Structure;
import com.sun.jna.ptr.PointerByReference;

/**
 * Access helpers for common JNA memory access features
 *
 * @author Oliver Broome
 */
public class MemoryUtils {
    public static void freePointer(Pointer pointer) {
        Native.free(PointerUtils.getAddress(pointer));
    }

    public static void freeStruct(Structure structure) {
        freePointer(structure.getPointer());
    }

    public static void freePointerRef(PointerByReference pointerByReference, boolean clearReferencedData) {
        if (clearReferencedData)
            freePointer(pointerByReference.getValue());

        freePointer(pointerByReference.getPointer());
    }
}
