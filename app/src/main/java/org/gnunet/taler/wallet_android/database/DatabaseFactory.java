/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

import org.gnunet.taler.wallet_android.database.storage.CoinsDatabase;
import org.gnunet.taler.wallet_android.database.storage.DenominationDatabase;
import org.gnunet.taler.wallet_android.database.storage.EncryptionDatabase;
import org.gnunet.taler.wallet_android.database.storage.MintSigningKeysDatabase;
import org.gnunet.taler.wallet_android.database.storage.MintsDatabase;
import org.gnunet.taler.wallet_android.database.storage.PurchasesDatabase;
import org.gnunet.taler.wallet_android.database.storage.RefreshCommitCoinDatabase;
import org.gnunet.taler.wallet_android.database.storage.RefreshCommitLinkDatabase;
import org.gnunet.taler.wallet_android.database.storage.RefreshDatabase;
import org.gnunet.taler.wallet_android.database.storage.RefreshMeltDatabase;
import org.gnunet.taler.wallet_android.database.storage.RefreshOrderDatabase;
import org.gnunet.taler.wallet_android.database.storage.RefreshSecretDatabase;
import org.gnunet.taler.wallet_android.database.storage.ReserveDatabase;
import org.gnunet.taler.wallet_android.database.storage.SpendsDatabase;
import org.gnunet.taler.wallet_android.database.storage.WithdrawDatabase;

/**
 * Factory for the different database tables
 *
 * @author Oliver Broome
 */
public class DatabaseFactory {
    public static final String DB_NAME = "taler-wallet.db";
    public static final int DB_VERSION = 1;
    public static final Object lock = new Object();

    private static DatabaseFactory instance;
    private final CoinsDatabase coins;
    private final EncryptionDatabase encryption;
    private final DenominationDatabase denominations;
    private final MintsDatabase mints;
    private final MintSigningKeysDatabase mintSigningKeys;
    private final PurchasesDatabase purchases;
    private final RefreshCommitCoinDatabase refreshCommitCoins;
    private final RefreshCommitLinkDatabase refreshCommitLinks;
    private final RefreshDatabase refreshes;
    private final RefreshMeltDatabase refreshMelts;
    private final RefreshOrderDatabase refreshOrders;
    private final RefreshSecretDatabase refreshSecrets;
    private final ReserveDatabase reserves;
    private final SpendsDatabase spends;
    private final WithdrawDatabase withdrawals;

    private DatabaseHelper databaseHelper;

    private DatabaseFactory(Context context) {
        this.databaseHelper = new DatabaseHelper(context, DB_NAME, null, DB_VERSION);

        this.coins = new CoinsDatabase(context, databaseHelper);
        this.denominations = new DenominationDatabase(context, databaseHelper);
        this.encryption = new EncryptionDatabase(context, databaseHelper);
        this.mints = new MintsDatabase(context, databaseHelper);
        this.mintSigningKeys = new MintSigningKeysDatabase(context, databaseHelper);
        this.purchases = new PurchasesDatabase(context, databaseHelper);
        this.refreshCommitCoins = new RefreshCommitCoinDatabase(context, databaseHelper);
        this.refreshCommitLinks = new RefreshCommitLinkDatabase(context, databaseHelper);
        this.refreshes = new RefreshDatabase(context, databaseHelper);
        this.refreshMelts = new RefreshMeltDatabase(context, databaseHelper);
        this.refreshOrders = new RefreshOrderDatabase(context, databaseHelper);
        this.refreshSecrets = new RefreshSecretDatabase(context, databaseHelper);
        this.reserves = new ReserveDatabase(context, databaseHelper);
        this.spends = new SpendsDatabase(context, databaseHelper);
        this.withdrawals = new WithdrawDatabase(context, databaseHelper);
    }

    public static DatabaseFactory getInstance(Context context) {
        synchronized (lock) {
            if (instance == null) {
                instance = new DatabaseFactory(context.getApplicationContext());
            }

            return instance;
        }
    }

    public static CoinsDatabase getCoinsDatabase(Context context) {
        return getInstance(context).coins;
    }

    public static DenominationDatabase getDenominationDatabase(Context context) {
        return getInstance(context).denominations;
    }

    public static EncryptionDatabase getEncryptionDatabase(Context context) {
        return getInstance(context).encryption;
    }

    public static MintsDatabase getMintsDatabase(Context context) {
        return getInstance(context).mints;
    }

    public static MintSigningKeysDatabase getMintSigningKeysDatabase(Context context) {
        return getInstance(context).mintSigningKeys;
    }

    public static PurchasesDatabase getPurchasesDatabase(Context context) {
        return getInstance(context).purchases;
    }

    public static RefreshCommitCoinDatabase getRefreshCommitCoinDatabase(Context context) {
        return getInstance(context).refreshCommitCoins;
    }

    public static RefreshCommitLinkDatabase getRefreshCommitLinkDatabase(Context context) {
        return getInstance(context).refreshCommitLinks;
    }

    public static RefreshDatabase getRefreshDatabase(Context context) {
        return getInstance(context).refreshes;
    }

    public static RefreshMeltDatabase getRefreshMeltDatabase(Context context) {
        return getInstance(context).refreshMelts;
    }

    public static RefreshOrderDatabase getRefreshOrderDatabase(Context context) {
        return getInstance(context).refreshOrders;
    }

    public static ReserveDatabase getReserveDatabase(Context context) {
        return getInstance(context).reserves;
    }

    public static SpendsDatabase getSpendsDatabase(Context context) {
        return getInstance(context).spends;
    }

    public static WithdrawDatabase getWithdrawDatabase(Context context) {
        return getInstance(context).withdrawals;
    }

    public void reset(Context context) {
        DatabaseHelper oldHelper = this.databaseHelper;

        this.databaseHelper = new DatabaseHelper(context, DB_NAME, null, DB_VERSION);

        this.coins.reset(databaseHelper);
        this.denominations.reset(databaseHelper);
        this.encryption.reset(databaseHelper);
        this.mints.reset(databaseHelper);
        this.mintSigningKeys.reset(databaseHelper);
        this.purchases.reset(databaseHelper);
        this.refreshCommitCoins.reset(databaseHelper);
        this.refreshCommitLinks.reset(databaseHelper);
        this.refreshes.reset(databaseHelper);
        this.refreshMelts.reset(databaseHelper);
        this.refreshOrders.reset(databaseHelper);
        this.refreshSecrets.reset(databaseHelper);
        this.reserves.reset(databaseHelper);
        this.spends.reset(databaseHelper);
        this.withdrawals.reset(databaseHelper);

        oldHelper.close();
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        public DatabaseHelper(Context context, String name, CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CoinsDatabase.TABLE_CREATE);
            db.execSQL(DenominationDatabase.TABLE_CREATE);
            db.execSQL(EncryptionDatabase.TABLE_CREATE);
            db.execSQL(MintsDatabase.TABLE_CREATE);
            db.execSQL(MintSigningKeysDatabase.TABLE_CREATE);
            db.execSQL(PurchasesDatabase.TABLE_CREATE);
            db.execSQL(RefreshCommitCoinDatabase.TABLE_CREATE);
            db.execSQL(RefreshCommitLinkDatabase.TABLE_CREATE);
            db.execSQL(RefreshDatabase.TABLE_CREATE);
            db.execSQL(RefreshMeltDatabase.TABLE_CREATE);
            db.execSQL(RefreshOrderDatabase.TABLE_CREATE);
            db.execSQL(RefreshSecretDatabase.TABLE_CREATE);
            db.execSQL(ReserveDatabase.TABLE_CREATE);
            db.execSQL(SpendsDatabase.TABLE_CREATE);
            db.execSQL(WithdrawDatabase.TABLE_CREATE);

            db.execSQL(CoinsDatabase.VIEW_CREATE);
        }

        @Override
        public void onConfigure(SQLiteDatabase db) {
            super.onConfigure(db);
            db.setForeignKeyConstraintsEnabled(true);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }
}