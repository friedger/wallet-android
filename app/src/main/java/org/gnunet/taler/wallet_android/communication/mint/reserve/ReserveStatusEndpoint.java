/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.communication.mint.reserve;

import android.support.annotation.NonNull;

import com.google.gson.stream.JsonReader;

import org.gnunet.taler.wallet_android.communication.exceptions.ReserveNotFoundException;
import org.gnunet.taler.wallet_android.communication.exceptions.UnknownJsonFieldException;
import org.gnunet.taler.wallet_android.database.model.objects.AmountDescriptor;
import org.gnunet.taler.wallet_android.database.model.state.ReserveHistoryDescriptor;
import org.gnunet.taler.wallet_android.database.model.state.WithdrawalHistoryDescriptor;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSAPublicKey;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import static org.gnunet.taler.wallet_android.communication.CommunicationConstants.METHOD_GET;
import static org.gnunet.taler.wallet_android.communication.mint.MintEndpointConstants.RESERVE_PREFIX;

/**
 * Represents the /reserve/status endpoint of the mint API.
 * <p/>
 * Allows the wallet to retrieve information about a reserve, such as the transaction history
 *
 * @author Oliver Broome
 */
public class ReserveStatusEndpoint extends AbstractReserveEndpoint {
    private static String encodedPublicKey;
    private AmountDescriptor balance;
    private List<ReserveHistoryDescriptor> reserveHistory;

    /**
     * Initialises a new reserve status endpoint
     *
     * @param baseURL          the mint's base URL
     * @param reservePublicKey the public key of the reserve to query
     * @throws IOException
     */
    public ReserveStatusEndpoint(@NonNull URL baseURL, EdDSAPublicKey reservePublicKey) throws IOException {
        super(baseURL,
                prepareURL(reservePublicKey),
                METHOD_GET);
    }

    /**
     * prepares the URL parameter for consumption by the constructor
     *
     * @param reservePublicKey the public key of the reserve to be queried
     * @return the prepared string with the encoded key
     */
    private static String prepareURL(EdDSAPublicKey reservePublicKey) {
        encodedPublicKey = reservePublicKey.encode();
        return RESERVE_PREFIX + "/status?reserve_pub=" + encodedPublicKey;
    }

    @Override
    public void parseResults(@NonNull JsonReader reader) throws IOException {
        reader.beginObject();

        while (reader.hasNext()) {
            switch (reader.nextName()) {
                case "balance":
                    balance = parseAmount(reader);
                    break;
                case "history":
                    reserveHistory = parseReserveHistory(reader);
                    break;
                default:
                    throw new UnknownJsonFieldException(reader);
            }
        }

        reader.endObject();

        reader.close();
    }

    @Override
    protected void handleErrors(int responseCode, @NonNull JsonReader reader) throws IOException {
        if (responseCode == 404) {
            reader.close();
            throw new ReserveNotFoundException(encodedPublicKey);
        } else {
            handleDefaultError(responseCode, reader);
        }
    }

    @Override
    public boolean verify() {
        List<ReserveHistoryDescriptor> results = getReserveHistory();

        for (ReserveHistoryDescriptor result : results) {
            if (result instanceof WithdrawalHistoryDescriptor) {
                //returns the withdrawal signature that was returned when the withdrawal was made
                //checking this doesn't make much sense right now.
            } else {
                //skip, since there is no signature on the deposit descriptors
            }
        }

        return true;
    }

    /**
     * Returns the new balance for the reserve
     *
     * @return an AmountDescriptor with the new balance
     */
    public AmountDescriptor getBalance() {
        return balance;
    }

    /**
     * Gets the list of reserve history descriptors from the response.
     *
     * @return the list of descriptors
     */
    public List<ReserveHistoryDescriptor> getReserveHistory() {
        return reserveHistory;
    }
}
