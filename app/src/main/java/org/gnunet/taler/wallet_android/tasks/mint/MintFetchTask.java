/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.tasks.mint;

import android.app.Activity;
import android.os.AsyncTask;

import org.gnunet.taler.wallet_android.database.DatabaseFactory;
import org.gnunet.taler.wallet_android.database.model.infrastructure.MintDescriptor;

import java.lang.ref.WeakReference;

/**
 * Task for fetching mint descriptors from the database
 *
 * @author Oliver Broome
 */
public class MintFetchTask extends AsyncTask<Void, Void, MintDescriptor> {
    private WeakReference<Activity> caller;
    private long mintId;

    public MintFetchTask(WeakReference<Activity> caller, long mintId) {
        this.caller = caller;
        this.mintId = mintId;
    }

    @Override
    protected void onPostExecute(MintDescriptor mintDescriptor) {
        Activity parent = caller.get();

        if (parent != null && parent instanceof Callbacks) {
            ((Callbacks) parent).receiveMintFromTask(mintDescriptor);
        }
    }

    @Override
    protected MintDescriptor doInBackground(Void... params) {
        Activity parent = caller.get();

        if (parent != null) {
            return DatabaseFactory.getMintsDatabase(parent.getApplicationContext()).getMintById(mintId);
        } else {
            return null;
        }
    }

    public interface Callbacks {
        void receiveMintFromTask(MintDescriptor mintDescriptor);
    }
}
