/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.communication.mint.test;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import org.gnunet.taler.wallet_android.communication.CommunicationConstants;
import org.gnunet.taler.wallet_android.communication.Endpoint;
import org.gnunet.taler.wallet_android.communication.exceptions.UnknownJsonFieldException;
import org.gnunet.taler.wallet_android.communication.mint.MintEndpointConstants;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EcDHEPrivateKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EcDHEPublicKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EcDHKeyMaterial;

import java.io.IOException;
import java.net.URL;

/**
 * Represents the /test/edche endpoint of the REST API
 *
 * @author Oliver Broome
 */
public class TestEcDHEEndpoint extends Endpoint {
    public String expectedResult;
    public String actualResult;

    public TestEcDHEEndpoint(@NonNull URL baseURL, EcDHEPrivateKey privateKey, EcDHEPublicKey publicKey) throws IOException {
        super(baseURL, MintEndpointConstants.TEST_PREFIX + "/ecdhe", CommunicationConstants.METHOD_POST);

        EcDHKeyMaterial keyMaterial = new EcDHKeyMaterial(privateKey, publicKey);

        expectedResult = keyMaterial.encode();

        JsonData postData = new JsonData(
                privateKey.encode(),
                publicKey.encode());

        Gson gson = new Gson();

        String postJson = gson.toJson(postData);

        prepareRequestBody(postJson);

        connect();
    }

    public String getActualResult() {
        return actualResult;
    }

    public String getExpectedResult() {
        return expectedResult;
    }

    @Override
    public void parseResults(@NonNull JsonReader reader) throws IOException {
        reader.beginObject();

        if ("ecdh_hash".equals(reader.nextName())) {
            actualResult = reader.nextString();
        } else {
            throw new UnknownJsonFieldException(reader);
        }

        reader.endObject();

        reader.close();
    }

    private class JsonData {
        public final String ecdhe_priv;
        private final String ecdhe_pub;

        public JsonData(String ecdhe_priv, String ecdhe_pub) {
            this.ecdhe_priv = ecdhe_priv;
            this.ecdhe_pub = ecdhe_pub;
        }
    }
}
