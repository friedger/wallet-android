/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.database.model.state;

import android.support.annotation.NonNull;

import org.gnunet.taler.wallet_android.database.model.objects.AmountDescriptor;
import org.gnunet.taler.wallet_android.database.model.objects.SepaDescriptor;

/**
 * Descriptor object for Withdrawal History Deposits
 *
 * @author Oliver Broome
 */
public class DepositHistoryDescriptor extends ReserveHistoryDescriptor {
    private SepaDescriptor wireInformation;

    public DepositHistoryDescriptor(@NonNull AmountDescriptor amount, @NonNull SepaDescriptor wireInformation) {
        super(amount);
        this.wireInformation = wireInformation;
    }

    public SepaDescriptor getWireInformation() {
        return wireInformation;
    }

    public void setWireInformation(SepaDescriptor wireInformation) {
        this.wireInformation = wireInformation;
    }

    @Override
    public void close() {
        if (wireInformation != null) {
            wireInformation.close();
            wireInformation = null;
        }
    }
}
