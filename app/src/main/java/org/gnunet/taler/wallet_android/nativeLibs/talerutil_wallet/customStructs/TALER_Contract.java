/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.talerutil_wallet.customStructs;

import com.sun.jna.Pointer;
import com.sun.jna.Structure;

import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNET_CRYPTO_EccSignaturePurpose;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNET_CRYPTO_EddsaSignature;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNET_HashCode;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNET_TIME_AbsoluteNBO;
import org.gnunet.taler.wallet_android.nativeLibs.talerutil_wallet.jna.TALER_AmountNBO;

import java.util.Arrays;
import java.util.List;

/**
 * Custom structure for Taler merchant contracts
 *
 * @author Oliver Broome
 */
public class TALER_Contract extends Structure {
    /**
     * the contract's signature
     */
    public GNUNET_CRYPTO_EddsaSignature sig;
    /**
     * the signature purpose for the signature
     */
    public GNUNET_CRYPTO_EccSignaturePurpose purpose;

    /**
     * the contract's ID
     */
    public byte[] m = new byte[8]; //contract ID, API definition unclear sticking with the equivalent of an uint64 for now.

    /**
     * the contract's creation time, network byte order
     */
    public GNUNET_TIME_AbsoluteNBO t;

    /**
     * the price defined by the contract
     */
    public TALER_AmountNBO amount;

    /**
     * the merchant's hashed wire information
     */
    public GNUNET_HashCode h_wire;

    /**
     * the merchant's description of the item covered by this contract
     */
    public byte[] a; //description

    public TALER_Contract() {
        super();
    }

    public TALER_Contract(Pointer peer) {
        super(peer);
    }

    public TALER_Contract(GNUNET_CRYPTO_EddsaSignature sig,
                          GNUNET_CRYPTO_EccSignaturePurpose purpose,
                          byte[] m, GNUNET_TIME_AbsoluteNBO t,
                          TALER_AmountNBO amount,
                          GNUNET_HashCode h_wire,
                          byte[] a) {
        this.sig = sig;
        this.purpose = purpose;
        this.m = m;
        this.t = t;
        this.amount = amount;
        this.h_wire = h_wire;
        this.a = a;

        allocateMemory();
    }

    public TALER_Contract(int descriptionLength) {
        a = new byte[descriptionLength];

        allocateMemory();
    }

    @Override
    protected List getFieldOrder() {
        return Arrays.asList("sig", "purpose", "m", "t", "amount", "h_wire", "a");
    }

    public static class ByReference extends TALER_Contract implements Structure.ByReference {
    }

    public static class ByValue extends TALER_Contract implements Structure.ByValue {
    }
}
