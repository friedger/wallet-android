/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.communication.exceptions;

import com.google.gson.stream.JsonReader;

import java.io.IOException;

/**
 * Exception that should be thrown when a reserve does not have enough funds in it to perform a transaction.
 *
 * @author Oliver Broome
 */
public class InsufficientFundsException extends IOException {
    public InsufficientFundsException(JsonReader reader) throws IOException {
        super(prepareMessage(reader));
    }

    private static String prepareMessage(JsonReader reader) throws IOException {
        //TODO: maybe do something with the reserve history and the amount that's missing
        return "Insufficient funds in the reserve for this transaction!";
    }
}
