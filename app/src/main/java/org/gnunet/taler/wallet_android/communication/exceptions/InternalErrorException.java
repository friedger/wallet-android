/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.communication.exceptions;

import android.support.annotation.NonNull;

import com.google.gson.stream.JsonReader;

import java.io.IOException;

/**
 * Exception for HTTP 500 errors
 *
 * @author Oliver Broome
 */
public class InternalErrorException extends IOException {

    public InternalErrorException(JsonReader reader) throws IOException {
        super(prepareMessage(reader));
    }

    private static String prepareMessage(@NonNull JsonReader reader) throws IOException {
        StringBuilder result = new StringBuilder(100);

        reader.beginObject();

        result.append("The server encountered an internal error:\n\t");

        while (reader.hasNext()) {
            switch (reader.nextName()) {
                case "error":
                    //this is always "internal error" so we skip it
                    break;
                case "hint":
                    result.append(reader.nextString());
                    break;
                default:
                    throw new UnknownJsonFieldException(reader);
            }
        }

        reader.endObject();

        reader.close();

        return result.toString();
    }
}
