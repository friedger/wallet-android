/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.utilities;

import java.math.BigInteger;
import java.util.Calendar;

/**
 * Utility class for microsecond-precision timestamps
 *
 * @author Oliver Broome
 */
public class MicrosecondTime {
    private static final BigInteger millisecondsDivisor = new BigInteger("1000");
    private BigInteger value;

    public MicrosecondTime(byte[] value, boolean networkByteOrdered) {
        if (networkByteOrdered) {
            this.value = new BigInteger(reverseBytes(value));
        } else {
            this.value = new BigInteger(value);
        }
    }

    private byte[] reverseBytes(byte[] input) {
        int length = input.length;

        if (length > 1) {

            int steps = length / 2;
            int opposite;
            byte temp;

            for (int i = 0; i < steps; i++) {
                opposite = length - i - 1;

                temp = input[i];
                input[i] = input[opposite];
                input[opposite] = temp;
            }
        }

        return input;
    }

    public Calendar getCalendar() {
        Calendar result = Calendar.getInstance();

        result.setTimeInMillis(value.divide(millisecondsDivisor).longValue());

        return result;
    }
}
