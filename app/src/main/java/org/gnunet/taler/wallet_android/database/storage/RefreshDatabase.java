/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.database.storage;

import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import static org.gnunet.taler.wallet_android.database.storage.DBConstants.AUTOINCREMENT;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.BEGIN_COLUMNS;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.BLOB;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.BOOLEAN;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.DEFAULT;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.END_COLUMNS;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.FALSE;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.INTEGER;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.NEXT_COLUMN;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.NOT_NULL;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.PRIMARY_KEY;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.UNIQUE;

/**
 * Allows access to the refresh table
 *
 * @author Oliver Broome
 */
public class RefreshDatabase extends AbstractDatabase {
    public static final String TABLE_NAME = "refresh";

    public static final String COLUMN_SESSION_PRIVATE = "session_priv";
    public static final String COLUMN_SESSION_PUBLIC = "session_pub";
    public static final String COLUMN_MELT_DONE = "melt_done";
    public static final String COLUMN_REVEAL_DONE = "reveal_done";
    public static final String COLUMN_KAPPA = "kappa";
    public static final String COLUMN_NOREVEAL_INDEX = "noreveal_index";

    public static final String TABLE_CREATE =
            "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + BEGIN_COLUMNS +
                    BaseColumns._ID + INTEGER + PRIMARY_KEY + AUTOINCREMENT + NEXT_COLUMN +
                    COLUMN_SESSION_PRIVATE + BLOB + UNIQUE + NOT_NULL + NEXT_COLUMN +
                    COLUMN_SESSION_PUBLIC + BLOB + UNIQUE + NOT_NULL + NEXT_COLUMN +
                    COLUMN_MELT_DONE + BOOLEAN + DEFAULT + FALSE + NEXT_COLUMN +
                    COLUMN_REVEAL_DONE + BOOLEAN + DEFAULT + FALSE + NEXT_COLUMN +
                    COLUMN_KAPPA + INTEGER + NEXT_COLUMN +
                    COLUMN_NOREVEAL_INDEX + INTEGER +
                    END_COLUMNS;

    public RefreshDatabase(Context context, SQLiteOpenHelper databaseHelper) {
        super(context, databaseHelper);
    }
}
