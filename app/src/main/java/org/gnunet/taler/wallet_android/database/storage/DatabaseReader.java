/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.database.storage;

import android.database.Cursor;
import android.support.annotation.NonNull;

import org.gnunet.taler.wallet_android.database.model.Descriptor;

/**
 * Common abstractions for database reader classes
 *
 * @author Oliver Broome
 */
public abstract class DatabaseReader implements AutoCloseable {
    protected final Cursor cursor;

    /**
     * Instantiates a new Reader for a given Cursor
     *
     * @param cursor the Cursor which this Reader should read from
     */
    public DatabaseReader(@NonNull Cursor cursor) {
        this.cursor = cursor;
    }

    /**
     * Returns the next Descriptor
     *
     * @return the next Descriptor object provided by the Cursor
     */
    public Descriptor getNext() {
        if (!cursor.moveToNext()) {
            return null;
        }

        return getCurrent();
    }

    /**
     * Returns the current Descriptor item
     *
     * @return the Descriptor item the Reader's cursor is currently pointing at
     */
    public abstract Descriptor getCurrent();

    /**
     * Returns the amount of items in the Reader's result set
     *
     * @return 0 if the result set is empty, or the number of items
     */
    public int getCount() {
        return cursor.getCount();
    }

    /**
     * Frees the resources associated with this Reader
     */
    @Override
    public void close() {
        cursor.close();
    }
}
