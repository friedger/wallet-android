#!/usr/bin/env bash
# this gives use NDK_TOOLCHAIN_VERSION, APP_ABI, etc.
NDK_TOOLCHAIN_VERSION="4.8"
APP_ABI="armeabi-v7a"

# the NDK platform level, aka APP_PLATFORM, is equivalent to minSdkVersion
APP_PLATFORM="15"

# Android NDK setup
NDK_ABI="arm"

# NDK platform level, aka APP_PLATFORM, is equivalent to minSdkVersion
NDK_SYSROOT=$ANDROID_NDK_HOME"/platforms/"$APP_PLATFORM"/arch-"$NDK_ABI
NDK_UNAME="linux"
NDK_PROCESSOR="x86_64"

HOST=$NDK_ABI"-linux-androideabi"
NDK_TOOLCHAIN=$HOST"-"$NDK_TOOLCHAIN_VERSION

NDK_TOOLCHAIN_BASE=$ANDROID_NDK_HOME"/toolchains/"$NDK_TOOLCHAIN"/prebuilt/"$NDK_UNAME"-"$NDK_PROCESSOR

NDK_PLATFORM=$ANDROID_NDK_HOME"/platforms/android-"$APP_PLATFORM"/arch-"$NDK_ABI

NDK_PLATFORM_INCLUDES_BASE=$NDK_PLATFORM"/usr/include"
NDK_PLATFORM_INCLUDES_LINUX=$NDK_PLATFORM_INCLUDES_BASE"/linux"

NDK_PLATFORM_INCLUDES=$NDK_PLATFORM_INCLUDES_BASE:$NDK_PLATFORM_INCLUDES_LINUX
NDK_TOOLCHAIN_INCLUDES=$NDK_TOOLCHAIN_BASE"/lib/gcc/"$HOST"/"$NDK_TOOLCHAIN_VERSION"/include/"

FINAL_INCLUDES=$NDK_PLATFORM_INCLUDES:$NDK_TOOLCHAIN_INCLUDES:~/workspace/wallet/android-wallet/app/src/main/jniLibs/artifacts/include

export JNAERATOR_INCLUDE_PATH=$FINAL_INCLUDES
export JNAERATOR_FRAMEWORKS_PATH=

echo "------------DEBUG----------"
echo "HOST:"
echo $HOST
echo "FINAL_INCLUDES"
echo $FINAL_INCLUDES
echo "------------DEBUG----------"

for dir in `ls -d */`
do
    cd $dir
    java -jar ~/Downloads/jnaerator.jar
    cd ..
done
