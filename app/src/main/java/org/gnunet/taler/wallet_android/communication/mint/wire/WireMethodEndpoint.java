/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.communication.mint.wire;

import android.support.annotation.NonNull;

import org.gnunet.taler.wallet_android.communication.FunctionalEndpoint;

import java.io.IOException;
import java.net.URL;

import static org.gnunet.taler.wallet_android.communication.CommunicationConstants.METHOD_GET;
import static org.gnunet.taler.wallet_android.communication.mint.MintEndpointConstants.WIRE_PREFIX;

/**
 * Represents the endpoint for a specific wire transfer method at /wire/*method*
 *
 * @author Oliver Broome
 */
public abstract class WireMethodEndpoint extends FunctionalEndpoint {
    /**
     * initialises the abstract wire method endpoint
     *
     * @param baseURL    the mint's base url
     * @param wireMethod the wire method endpoint to call
     * @throws IOException
     */
    public WireMethodEndpoint(@NonNull URL baseURL, String wireMethod) throws IOException {
        super(baseURL, WIRE_PREFIX + wireMethod, METHOD_GET);
    }
}
