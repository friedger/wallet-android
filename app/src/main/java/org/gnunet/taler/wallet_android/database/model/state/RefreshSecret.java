/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.database.model.state;

/**
 * Descriptor for the RefreshSecret table
 *
 * @author Oliver Broome
 */
public class RefreshSecret {
    /**
     * The Refresh this RefreshSecret is associated with
     */
    private Refresh refresh;

    /**
     * The secret to be used for the Refresh
     */
    private byte[] secret;

    /**
     * //TODO: Find out what this is
     */
    private int cncIndex;

    public Refresh getRefresh() {
        return refresh;
    }

    public void setRefresh(Refresh refresh) {
        this.refresh = refresh;
    }

    public byte[] getSecret() {
        return secret;
    }

    public void setSecret(byte[] secret) {
        this.secret = secret;
    }

    public int getCncIndex() {
        return cncIndex;
    }

    public void setCncIindex(int cncIndex) {
        this.cncIndex = cncIndex;
    }
}
