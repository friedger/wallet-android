/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.database.model.state;

import java.security.PrivateKey;

/**
 * Represents the encryption settings for the database
 *
 * @author Oliver Broome
 */
public class EncryptionDescriptor {
    private int one;
    private PrivateKey masterKey;
    private byte[] salt;
    private byte[] scryptHash;
    private int scryptSubAlgorithm = 200;     //FIXME: the parameter n = 200 does not represent any valid hashing algorithm in libgcrypt
    private int scryptIterations = 2;      //p
    private int scryptBlockSize = 8;    //r, must remain fixed due to scrypt limitations
}
