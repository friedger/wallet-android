/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.communication.mint.wire;

import android.support.annotation.NonNull;

import com.google.gson.stream.JsonReader;

import org.gnunet.taler.wallet_android.database.model.objects.SepaDescriptor;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.ECCSignaturePurpose;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSAPublicKey;

import java.io.IOException;
import java.net.URL;

import static org.gnunet.taler.wallet_android.nativeLibs.talerutil_wallet.jna.TalerLibrary.Taler_Signatures.TALER_SIGNATURE_MASTER_SEPA_DETAILS;

/**
 * Wrapper for the /wire/sepa endpoint of the REST API
 *
 * @author Oliver Broome
 */
public class WireSepaEndpoint extends WireMethodEndpoint {
    private EdDSAPublicKey publicKey;
    private SepaDescriptor wireInformation;

    /**
     * initialises a SEPA wire method endpoint
     *
     * @param baseURL       the mint's base URL
     * @param mintPublicKey the mint's public signing key, used for verification of the response
     * @throws IOException
     */
    public WireSepaEndpoint(@NonNull URL baseURL, @NonNull EdDSAPublicKey mintPublicKey) throws IOException {
        super(baseURL, "/sepa");
        this.publicKey = mintPublicKey;
    }

    @Override
    protected void parseResults(@NonNull JsonReader reader) throws IOException {
        wireInformation = parseSepaDetails(reader);
        reader.close();
    }

    @Override
    public boolean verify() {
        try (ECCSignaturePurpose signaturePurpose = new ECCSignaturePurpose(
                new String[]{
                        wireInformation.getRecipient(),
                        wireInformation.getIban(),
                        wireInformation.getBic()},
                true,
                TALER_SIGNATURE_MASTER_SEPA_DETAILS)) {

            boolean result = wireInformation.getSignature().verify(signaturePurpose, publicKey);

            wireInformation.close();

            return result;
        }

    }

    /**
     * returns the SEPA wire information descriptor returned by the mint
     *
     * @return
     */
    public SepaDescriptor getWireInformation() {
        return wireInformation;
    }
}
