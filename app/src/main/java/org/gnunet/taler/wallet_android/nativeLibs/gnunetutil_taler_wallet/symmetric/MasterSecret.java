/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.symmetric;

import android.os.Parcel;
import android.os.Parcelable;

import com.ochafik.lang.jnaerator.runtime.NativeSize;
import com.sun.jna.Memory;

import org.gnunet.taler.wallet_android.utilities.JNAhelpers.MemoryUtils;

/**
 * Wrapper class for a derived master secret, which is used to encrypt private keys in the database.
 * Currently mimics the behaviour of the python wallet.
 */
public class MasterSecret implements Parcelable, AutoCloseable {
    public static final Creator<MasterSecret> CREATOR = new Creator<MasterSecret>() {
        @Override
        public MasterSecret createFromParcel(Parcel in) {
            return new MasterSecret(in);
        }

        @Override
        public MasterSecret[] newArray(int size) {
            return new MasterSecret[size];
        }
    };

    private Memory key;

    public MasterSecret(String passphrase) {
        SessionKey derivedSessionKey = new SessionKey(passphrase, new String[]{"walletpw"});
        SessionKey masterKey = new SessionKey();
        InitialisationVector initialisationVector = derivedSessionKey.deriveIV(new String[]{"walletkey"});

        Memory encryptedMasterKey = derivedSessionKey.encrypt(
                masterKey.getData().getPointer(),
                new NativeSize(masterKey.getData().size()),
                initialisationVector
        );


    }

    protected MasterSecret(Parcel in) {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    @Override
    public void close() {
        if (key != null) {
            key.clear();
            MemoryUtils.freePointer(key);

            key = null;
        }
    }
}
