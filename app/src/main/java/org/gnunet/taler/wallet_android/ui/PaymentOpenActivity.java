/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import org.gnunet.taler.wallet_android.R;
import org.gnunet.taler.wallet_android.utilities.scanIntents.IntentIntegrator;
import org.gnunet.taler.wallet_android.utilities.scanIntents.IntentResult;

/**
 * Activity for scanning a contract QR code
 *
 * @author Oliver Broome
 */
public class PaymentOpenActivity extends AppCompatActivity {

    public final static String urlMessage = "org.gnunet.taler.wallet_android.CONTRACT_URL";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_open);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_payment_open, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        EditText urlField = (EditText) findViewById(R.id.contractURL);

        super.onActivityResult(requestCode, resultCode, intent);

        String data = scanResult.getContents();

        if ((scanResult != null) && (data != null)) {
            urlField.setText(data);
        } else {
            urlField.setText("No valid data!");
        }
    }

    private IntentIntegrator getIntentIntegrator() {
        IntentIntegrator intentIntegrator = new IntentIntegrator(this);
        intentIntegrator.setButtonYesByID(R.string.yes);
        intentIntegrator.setButtonNoByID(R.string.no);
        intentIntegrator.setTitleByID(R.string.KeyScanningActivity_install_barcode_Scanner);
        intentIntegrator.setMessageByID(R.string.KeyScanningActivity_this_application_requires_barcode_scanner_would_you_like_to_install_it);
        return intentIntegrator;
    }

    public void initiateScan(View view) {
        IntentIntegrator intentIntegrator = getIntentIntegrator();
        intentIntegrator.initiateScan();
    }

    public void launchPaymentDetailActivity(View view) {
        Intent intent = new Intent(this, PaymentDetailActivity.class);
        EditText urlField = (EditText) findViewById(R.id.contractURL);

        intent.putExtra(urlMessage, urlField.getText());
        startActivity(intent);
    }
}
