/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc;

import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNET_CRYPTO_EcdhePublicKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNetLibrary;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.utilities.Codec;
import org.gnunet.taler.wallet_android.utilities.JNAhelpers.MemoryUtils;

/**
 * Wrapper for ECDH public keys
 *
 * @author Oliver Broome
 */
public class EcDHEPublicKey implements AutoCloseable {
    private GNUNET_CRYPTO_EcdhePublicKey keyStruct;

    /**
     * Generates a private ECDH key from the given private key.
     *
     * @param privateKey the private key to generate the public key for
     */
    public EcDHEPublicKey(EcDHEPrivateKey privateKey) {
        keyStruct = new GNUNET_CRYPTO_EcdhePublicKey();
        GNUNetLibrary.GNUNET_CRYPTO_ecdhe_key_get_public(privateKey.getStruct(), keyStruct);
    }

    /**
     * Creates a public EcDHE key from an encoded representation.
     *
     * @param gnunetEncoded the base32-encoded key data
     */
    public EcDHEPublicKey(String gnunetEncoded) {
        this.keyStruct = new GNUNET_CRYPTO_EcdhePublicKey(Codec.decode(gnunetEncoded));
        keyStruct.read();
    }

    /**
     * Gets the key's underlying Structure object.
     *
     * @return the GNUNET_CRYPTO_EcdhePublicKey Structure object
     */
    public GNUNET_CRYPTO_EcdhePublicKey getStruct() {
        if (keyStruct != null) {
            return keyStruct;
        }
        throw new RuntimeException("The memory associated with this key has been freed.");
    }

    /**
     * Encodes the public key as a String, using Base32
     *
     * @return the encoded String
     */
    public String encode() {
        return Codec.encode(keyStruct);
    }

    /**
     * Frees the memory associated with this key.
     */
    @Override
    public void close() {
        if (keyStruct != null) {
            keyStruct.clear();
            MemoryUtils.freeStruct(keyStruct);
            keyStruct = null;
        }
    }
}
