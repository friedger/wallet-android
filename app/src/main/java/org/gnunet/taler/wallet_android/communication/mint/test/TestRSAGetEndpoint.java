/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.communication.mint.test;

import android.support.annotation.NonNull;

import com.google.gson.stream.JsonReader;

import org.gnunet.taler.wallet_android.communication.Endpoint;
import org.gnunet.taler.wallet_android.communication.exceptions.UnknownJsonFieldException;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa.RSAPublicKey;

import java.io.IOException;
import java.net.URL;

import static org.gnunet.taler.wallet_android.communication.CommunicationConstants.METHOD_GET;
import static org.gnunet.taler.wallet_android.communication.mint.MintEndpointConstants.TEST_PREFIX;

/**
 * Represents the /test/rsa/get endpoint of the mint API.
 * <p/>
 * Obtains the public RSA key used for testing from the mint.
 *
 * @author Oliver Broome
 */
public class TestRSAGetEndpoint extends Endpoint {
    private RSAPublicKey receivedPublicKey;

    public TestRSAGetEndpoint(@NonNull URL baseURL) throws IOException {
        super(baseURL, TEST_PREFIX + "/rsa/get", METHOD_GET);
    }

    @Override
    public void parseResults(@NonNull JsonReader reader) throws IOException {
        reader.beginObject();

        if ("rsa_pub".equals(reader.nextName())) {
            receivedPublicKey = new RSAPublicKey(reader.nextString(), true);
        } else {
            throw new UnknownJsonFieldException(reader);
        }

        reader.endObject();

        reader.close();
    }

    public RSAPublicKey getReceivedPublicKey() {
        return receivedPublicKey;
    }
}
