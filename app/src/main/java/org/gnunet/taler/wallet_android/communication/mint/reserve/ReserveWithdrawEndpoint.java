/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.communication.mint.reserve;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.ochafik.lang.jnaerator.runtime.NativeSize;

import org.gnunet.taler.wallet_android.communication.exceptions.InsufficientFundsException;
import org.gnunet.taler.wallet_android.communication.exceptions.InvalidSignatureException;
import org.gnunet.taler.wallet_android.communication.exceptions.ReserveNotFoundException;
import org.gnunet.taler.wallet_android.database.model.objects.CoinDescriptor;
import org.gnunet.taler.wallet_android.database.model.objects.ReserveDescriptor;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.ECCSignaturePurpose;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSASignature;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa.RSABlindedMessage;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa.RSABlindingKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa.RSAPublicKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa.RSASignature;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.utilities.Hash;
import org.gnunet.taler.wallet_android.nativeLibs.talerutil_wallet.jna.TalerLibrary;

import java.io.IOException;
import java.net.URL;

import static org.gnunet.taler.wallet_android.communication.CommunicationConstants.METHOD_POST;
import static org.gnunet.taler.wallet_android.communication.mint.MintEndpointConstants.RESERVE_PREFIX;

/**
 * Represents the /reserve/withdraw endpoint of the mint API.
 * <p/>
 * Allows the wallet to withdraw a coin from the mint.
 *
 * @author Oliver Broome
 */
public class ReserveWithdrawEndpoint extends AbstractReserveEndpoint {
    private RSASignature coinSignature;
    private RSAPublicKey publicDenominationKey;
    private Hash coinKeyHash;
    private RSABlindingKey blindingKey;

    private String reservePublicKey;
    private String denominationPublicKey;

    public ReserveWithdrawEndpoint(@NonNull URL baseURL, @NonNull CoinDescriptor coin, ReserveDescriptor reserve) throws IOException {
        super(baseURL, RESERVE_PREFIX + "/withdraw", METHOD_POST);

        this.reservePublicKey = reserve.getPublicKey().encode();
        this.denominationPublicKey = coin.getDenomination().getPublicKey().encode();

        this.publicDenominationKey = coin.getDenomination().getPublicKey();

        blindingKey = new RSABlindingKey(2048);

        coinKeyHash = new Hash(
                coin.getPrivateKey().getStruct().getPointer(),
                new NativeSize(256));

        try (ECCSignaturePurpose signaturePurpose = new ECCSignaturePurpose(
                null, 0,
                TalerLibrary.Taler_Signatures.TALER_SIGNATURE_WALLET_RESERVE_WITHDRAW);
             RSABlindedMessage blindedCoinKey = new RSABlindedMessage(
                     coinKeyHash,
                     blindingKey,
                     coin.getDenomination().getPublicKey());
             EdDSASignature signature = new EdDSASignature(
                     signaturePurpose,
                     reserve.getPrivateKey())) {

            PostData postData = new PostData(
                    coin.getDenomination().getPublicKey().encode(),
                    blindedCoinKey.encode(),
                    reserve.getPublicKey().encode(),
                    signature.encode()
            );

            Gson gson = new Gson();

            prepareRequestBody(gson.toJson(postData));
        }
    }

    public RSABlindingKey getBlindingKey() {
        return blindingKey;
    }

    @Override
    public void parseResults(@NonNull JsonReader reader) throws IOException {
        reader.beginObject();

        if ("coin_ev".equals(reader.nextName())) {
            coinSignature = new RSASignature(reader.nextString(), true);
        }

        reader.endObject();

        reader.close();
    }

    @Override
    protected void handleErrors(int responseCode, @NonNull JsonReader reader) throws IOException {
        switch (responseCode) {
            case 401:
                throw new InvalidSignatureException("reserve", reader);
            case 402:
                throw new InsufficientFundsException(reader);
            case 404:
                throw new ReserveNotFoundException(denominationPublicKey, reservePublicKey, reader);
            default:
                handleDefaultError(responseCode, reader);
                break;
        }
    }

    @Override
    public boolean verify() {
        try (RSASignature unblindedSignature = new RSASignature(
                coinSignature,
                blindingKey,
                publicDenominationKey)) {
            return unblindedSignature.verify(coinKeyHash, publicDenominationKey);
        }
    }

    public RSASignature getCoinSignature() {
        return coinSignature;
    }

    private class PostData {
        String denom_pub;
        String coin_ev;
        String reserve_pub;
        String reserve_sig;

        public PostData(String denom_pub, String coin_ev, String reserve_pub, String reserve_sig) {
            this.denom_pub = denom_pub;
            this.coin_ev = coin_ev;
            this.reserve_pub = reserve_pub;
            this.reserve_sig = reserve_sig;
        }
    }
}
