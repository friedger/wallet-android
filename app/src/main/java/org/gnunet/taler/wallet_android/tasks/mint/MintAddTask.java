/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.tasks.mint;

import android.os.AsyncTask;
import android.util.Log;

import org.gnunet.taler.wallet_android.communication.mint.KeysEndpoint;
import org.gnunet.taler.wallet_android.database.DatabaseFactory;
import org.gnunet.taler.wallet_android.database.model.infrastructure.MintDescriptor;
import org.gnunet.taler.wallet_android.database.model.infrastructure.MintSigningKeyDescriptor;
import org.gnunet.taler.wallet_android.database.model.objects.DenominationDescriptor;
import org.gnunet.taler.wallet_android.database.storage.DenominationDatabase;
import org.gnunet.taler.wallet_android.database.storage.MintSigningKeysDatabase;
import org.gnunet.taler.wallet_android.database.storage.MintsDatabase;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSAPublicKey;
import org.gnunet.taler.wallet_android.ui.fragments.MintAddWorkerFragment;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.util.List;

/**
 * Task for adding mints to the wallet
 *
 * @author Oliver Broome
 */
public class MintAddTask extends AsyncTask<MintDescriptor, Integer, Long[]> {
    private WeakReference<MintAddWorkerFragment> caller;

    public MintAddTask(WeakReference<MintAddWorkerFragment> caller) {
        this.caller = caller;
    }

    @Override
    protected Long[] doInBackground(MintDescriptor... params) {
        MintAddWorkerFragment workerFragment = caller.get();
        Long[] result = new Long[params.length];

        if (workerFragment != null) {
            for (int i = 0; i < params.length; i++) {
                MintsDatabase mintsDb =
                        DatabaseFactory.getMintsDatabase(workerFragment.getContext());
                MintDescriptor mint = params[i];

                try (KeysEndpoint endpoint = new KeysEndpoint(mint.getUrl())) {
                    endpoint.connect();

                    final EdDSAPublicKey publicMasterKey = endpoint.getPublicMasterKey();

                    if (publicMasterKey != null) {
                        long mintId = mintsDb.addMint(
                                mint.getName(),
                                mint.getUrl(),
                                publicMasterKey);

                        final List<MintSigningKeyDescriptor> signingKeys = endpoint.getSigningKeys();
                        final List<DenominationDescriptor> denominationDescriptors = endpoint.getDenominations();

                        if (mintId >= 0) {
                            DenominationDatabase denominationDB =
                                    DatabaseFactory.getDenominationDatabase(workerFragment.getContext());
                            MintSigningKeysDatabase signingKeysDB =
                                    DatabaseFactory.getMintSigningKeysDatabase(workerFragment.getContext());

                            if (!signingKeys.isEmpty()) {
                                MintUpdateTask.saveSigningKeys(
                                        mintId,
                                        signingKeysDB,
                                        signingKeys);
                            }

                            if (!signingKeys.isEmpty()) {
                                MintUpdateTask.saveDenominationKeys(
                                        mintId,
                                        denominationDB,
                                        denominationDescriptors);
                            }

                            result[i] = mintId;
                        } else {
                            result[i] = -1L;
                        }
                    }
                } catch (MalformedURLException e) {
                    Log.w("taler_mint_add", "Mint descriptor had a bad URL!", e);
                } catch (IOException e) {
                    Log.w("taler_mint_add", "Received invalid data from the given URL!", e);
                }
            }
        }

        return result;
    }

    @Override
    protected void onPostExecute(Long[] longs) {
        super.onPostExecute(longs);

        MintAddWorkerFragment parent = caller.get();

        if (parent != null) {
            parent.endTask(longs);
        }
    }
}
