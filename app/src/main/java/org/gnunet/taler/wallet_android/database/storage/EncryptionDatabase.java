/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.database.storage;

import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;

import static org.gnunet.taler.wallet_android.database.storage.DBConstants.BEGIN_COLUMNS;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.BLOB;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.CHECK;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.DEFAULT;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.END_COLUMNS;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.INTEGER;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.NEXT_COLUMN;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.NOT_NULL;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.UNIQUE;

/**
 * Allows access to the encryption table
 *
 * @author Oliver Broome
 */
public class EncryptionDatabase extends AbstractDatabase {
    public static final String TABLE_NAME = "encryption";

    public static final String COLUMN_ONE = "one";
    public static final String COLUMN_MASTER_KEY_ENCODED = "master_key_enc";
    public static final String COLUMN_SALT = "salt";
    public static final String COLUMN_SCRYPT_HASH = "scrypt_hash";
    public static final String COLUMN_SCRYPT_N = "scrypt_n";
    public static final String COLUMN_SCRYPT_P = "scrypt_p";
    public static final String COLUMN_SCRYPT_R = "scrypt_r";

    public static final String TABLE_CREATE =
            "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + BEGIN_COLUMNS +
                    COLUMN_ONE + INTEGER + UNIQUE + CHECK + "(" + COLUMN_ONE + " = 1) " + NOT_NULL + DEFAULT + "1" + NEXT_COLUMN +
                    COLUMN_MASTER_KEY_ENCODED + BLOB + NEXT_COLUMN +
                    COLUMN_SALT + BLOB + NEXT_COLUMN +
                    COLUMN_SCRYPT_HASH + BLOB + NEXT_COLUMN +
                    COLUMN_SCRYPT_N + INTEGER + NEXT_COLUMN +
                    COLUMN_SCRYPT_P + INTEGER + NEXT_COLUMN +
                    COLUMN_SCRYPT_R + INTEGER +
                    END_COLUMNS;

    public EncryptionDatabase(Context context, SQLiteOpenHelper databaseHelper) {
        super(context, databaseHelper);
    }
}
