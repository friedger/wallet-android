/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.gnunet.taler.wallet_android.R;
import org.gnunet.taler.wallet_android.utilities.StableArrayAdapter;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Activity for showing mints added to the wallet
 *
 * @author Oliver Broome
 */
public class MintListActivity extends AppCompatActivity {

    ListView mintListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_mint);

        mintListView = (ListView) findViewById(R.id.mintList);

        String[] values = new String[]{"Mint 1", "Mint 2", "Mint 3", "Mint 4"};
        final ArrayList<String> list = new ArrayList<>();
        Collections.addAll(list, values);

        final StableArrayAdapter adapter = new StableArrayAdapter(this,
                android.R.layout.simple_list_item_1, list);

        mintListView.setAdapter(adapter);
        mintListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(view.getContext(), MintDetailActivity.class);

                intent.putExtra(IntentConstants.EXTRA_MINT_ID, id);

                startActivity(intent);
            }
        });
    }

    public void launchMintAddActivity(View view) {
        Intent intent = new Intent(view.getContext(), MintAddActivity.class);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK); //return to this activity when adding a mint.

        startActivity(intent);
        finish();
    }
}
