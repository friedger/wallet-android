/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.database.storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import org.gnunet.taler.wallet_android.database.model.objects.AmountDescriptor;
import org.gnunet.taler.wallet_android.database.model.objects.ReserveDescriptor;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSAPrivateKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSAPublicKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSASignature;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa.RSABlindingKey;

import static org.gnunet.taler.wallet_android.database.storage.DBConstants.AUTOINCREMENT;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.BEGIN_COLUMNS;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.BLOB;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.END_COLUMNS;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.INTEGER;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.NEXT_COLUMN;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.NOT_NULL;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.ON_DELETE;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.PRIMARY_KEY;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.REFERENCES;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.SET_NULL;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.TEXT;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.WHERE;

/**
 * Allows access to the reserve table
 *
 * @author Oliver Broome
 */
public class ReserveDatabase extends AbstractDatabase {
    public static final String TABLE_NAME = "reserves";

    public static final String COLUMN_MINT_ID = "mintid";

    public static final String COLUMN_X_RESERVE_PRIV = "x_reserve_priv";
    public static final String COLUMN_RESERVE_PUB = "reserve_pub";

    public static final String COLUMN_CURRENCY = "currency";
    public static final String COLUMN_INITIAL_VALUE = "initial_value";
    public static final String COLUMN_INITIAL_FRACTION = "initial_fraction";

    public static final String COLUMN_CURRENT_BLIND_SESSION_PUB = "current_blind_session_pub";
    public static final String COLUMN_CURRENT_STATUS_SIG = "current_status_sig";

    public static final String COLUMN_CURRENT_VALUE = "current_value";
    public static final String COLUMN_CURRENT_FRACTION = "current_fraction";
    public static final String COLUMN_CURRENT_CURRENCY = "current_currency";

    public static final String TABLE_CREATE =
            "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + BEGIN_COLUMNS +
                    BaseColumns._ID + INTEGER + PRIMARY_KEY + AUTOINCREMENT + NEXT_COLUMN +
                    COLUMN_MINT_ID + INTEGER + NOT_NULL + REFERENCES + MintsDatabase.TABLE_NAME + " (" + BaseColumns._ID + ")" + ON_DELETE + SET_NULL + NEXT_COLUMN +
                    COLUMN_X_RESERVE_PRIV + BLOB + NOT_NULL + NEXT_COLUMN +
                    COLUMN_RESERVE_PUB + BLOB + NOT_NULL + NEXT_COLUMN +
                    COLUMN_CURRENCY + TEXT + NOT_NULL + NEXT_COLUMN +
                    COLUMN_INITIAL_VALUE + INTEGER + NOT_NULL + NEXT_COLUMN +
                    COLUMN_INITIAL_FRACTION + INTEGER + NOT_NULL + NEXT_COLUMN +
                    COLUMN_CURRENT_BLIND_SESSION_PUB + BLOB + NEXT_COLUMN +
                    COLUMN_CURRENT_STATUS_SIG + BLOB + NEXT_COLUMN +
                    COLUMN_CURRENT_VALUE + INTEGER + NEXT_COLUMN +
                    COLUMN_CURRENT_FRACTION + INTEGER + NEXT_COLUMN +
                    COLUMN_CURRENT_CURRENCY + TEXT +
                    END_COLUMNS;

    public static final String[] RESERVE_PROJECTION = new String[]{
            COLUMN_MINT_ID,
            COLUMN_X_RESERVE_PRIV, COLUMN_RESERVE_PUB,
            COLUMN_CURRENCY, COLUMN_INITIAL_VALUE, COLUMN_INITIAL_FRACTION,
            COLUMN_CURRENT_BLIND_SESSION_PUB, COLUMN_CURRENT_STATUS_SIG,
            COLUMN_CURRENT_VALUE, COLUMN_CURRENT_FRACTION, COLUMN_CURRENT_CURRENCY
    };

    public static final String MINT_ID_WHERE = WHERE + COLUMN_MINT_ID + " = ?";
    public static final String RESERVE_ID_WHERE = WHERE + BaseColumns._ID + " = ?";

    public ReserveDatabase(Context context, SQLiteOpenHelper databaseHelper) {
        super(context, databaseHelper);
    }

    public long addReserve(ReserveDescriptor reserve) {
        try (SQLiteDatabase database = databaseHelper.getWritableDatabase()) {
            ContentValues values = new ContentValues(11);

            values.put(COLUMN_MINT_ID, reserve.getMintId());

            values.put(COLUMN_X_RESERVE_PRIV, reserve.getPrivateKey().getStruct().d); //FIXME: needs to be encrypted!
            values.put(COLUMN_RESERVE_PUB, reserve.getPublicKey().getStruct().q_y);

            values.put(COLUMN_CURRENCY, reserve.getCurrentValue().getCurrency());

            values.put(COLUMN_INITIAL_VALUE, reserve.getInitialValue().getValue());
            values.put(COLUMN_INITIAL_FRACTION, reserve.getInitialValue().getFraction());
            values.put(COLUMN_CURRENT_VALUE, reserve.getCurrentValue().getValue());
            values.put(COLUMN_CURRENT_FRACTION, reserve.getCurrentValue().getFraction());
            values.put(COLUMN_CURRENT_CURRENCY, reserve.getCurrentValue().getCurrency());

            return database.insert(TABLE_NAME, null, values);
        }
    }

    public Cursor getReservesForMint(long mintId) {
        SQLiteDatabase database = databaseHelper.getReadableDatabase();

        return database.query(TABLE_NAME,
                RESERVE_PROJECTION,
                MINT_ID_WHERE,
                new String[]{Long.toString(mintId)},
                null, null, null);

    }

    public boolean removeReserve(long reserveId) {
        try (SQLiteDatabase database = databaseHelper.getWritableDatabase()) {
            return database.delete(TABLE_NAME,
                    RESERVE_ID_WHERE,
                    new String[]{Long.toString(reserveId)}) == 1;
        }
    }

    public boolean updateBalance(long reserveId, AmountDescriptor newBalance) {
        try (SQLiteDatabase database = databaseHelper.getWritableDatabase()) {
            ContentValues values = new ContentValues(3);

            values.put(COLUMN_CURRENT_CURRENCY, newBalance.getCurrency());
            values.put(COLUMN_CURRENT_VALUE, newBalance.getValue());
            values.put(COLUMN_CURRENT_FRACTION, newBalance.getFraction());

            return database.update(TABLE_NAME,
                    values,
                    RESERVE_ID_WHERE,
                    new String[]{Long.toString(reserveId)}) == 1;
        }
    }

    public Reader readerFor(Cursor cursor) {
        return new Reader(cursor);
    }

    public class Reader extends DatabaseReader {
        public Reader(Cursor cursor) {
            super(cursor);
        }

        @Override
        public ReserveDescriptor getCurrent() {
            long id = cursor.getLong(cursor.getColumnIndexOrThrow(BaseColumns._ID));
            long mintId = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_MINT_ID));


            EdDSAPrivateKey privateKey = new EdDSAPrivateKey(
                    cursor.getBlob(
                            cursor.getColumnIndexOrThrow(COLUMN_X_RESERVE_PRIV)));
            EdDSAPublicKey publicKey = new EdDSAPublicKey(
                    cursor.getBlob(
                            cursor.getColumnIndexOrThrow(COLUMN_RESERVE_PUB)));

            EdDSASignature statusSignature = new EdDSASignature(
                    null, cursor.getBlob(cursor.getColumnIndexOrThrow(COLUMN_CURRENT_STATUS_SIG)));
            RSABlindingKey blindingKey = new RSABlindingKey(
                    cursor.getBlob(
                            cursor.getColumnIndexOrThrow(COLUMN_CURRENT_BLIND_SESSION_PUB)));

            AmountDescriptor initialValue = new AmountDescriptor(
                    cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_CURRENCY)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_INITIAL_VALUE)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_INITIAL_FRACTION))
            );
            AmountDescriptor currentValue = new AmountDescriptor(
                    cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_CURRENT_CURRENCY)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_CURRENT_VALUE)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_CURRENT_FRACTION))
            );

            return new ReserveDescriptor(id, mintId,
                    privateKey, publicKey,
                    initialValue, currentValue,
                    blindingKey, statusSignature);
        }
    }
}
