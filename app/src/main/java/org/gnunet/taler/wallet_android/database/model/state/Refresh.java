/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.database.model.state;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.List;

/**
 * Descriptor for the refresh table
 *
 * @author Oliver Broome
 */
public class Refresh {
    private int refreshID;
    private PrivateKey privateSessionKey;
    private PublicKey publicSessionKey;
    private boolean melted;
    private boolean revealed;
    private int kappa;
    private int noRevealIndex;

    private List<RefreshOrder> refreshOrders;
    private List<RefreshMelt> refreshMelts;
    private List<RefreshSecret> refreshSecrets;
    private List<RefreshCommitLink> refreshCommitLinks;
    private List<RefreshCommitCoin> refreshCommitCoins;

    public int getRefreshID() {
        return refreshID;
    }

    public void setRefreshID(int refreshID) {
        this.refreshID = refreshID;
    }

    public PrivateKey getPrivateSessionKey() {
        return privateSessionKey;
    }

    public void setPrivateSessionKey(PrivateKey privateSessionKey) {
        this.privateSessionKey = privateSessionKey;
    }

    public PublicKey getPublicSessionKey() {
        return publicSessionKey;
    }

    public void setPublicSessionKey(PublicKey publicSessionKey) {
        this.publicSessionKey = publicSessionKey;
    }

    public boolean isMelted() {
        return melted;
    }

    public void setMelted(boolean melted) {
        this.melted = melted;
    }

    public boolean isRevealed() {
        return revealed;
    }

    public void setRevealed(boolean revealed) {
        this.revealed = revealed;
    }

    public int getKappa() {
        return kappa;
    }

    public void setKappa(int kappa) {
        this.kappa = kappa;
    }

    public int getNoRevealIndex() {
        return noRevealIndex;
    }

    public void setNoRevealIndex(int noRevealIndex) {
        this.noRevealIndex = noRevealIndex;
    }

    public List<RefreshOrder> getRefreshOrders() {
        return refreshOrders;
    }

    public void setRefreshOrders(List<RefreshOrder> refreshOrders) {
        this.refreshOrders = refreshOrders;
    }

    public List<RefreshMelt> getRefreshMelts() {
        return refreshMelts;
    }

    public void setRefreshMelts(List<RefreshMelt> refreshMelts) {
        this.refreshMelts = refreshMelts;
    }

    public List<RefreshSecret> getRefreshSecrets() {
        return refreshSecrets;
    }

    public void setRefreshSecrets(List<RefreshSecret> refreshSecrets) {
        this.refreshSecrets = refreshSecrets;
    }

    public List<RefreshCommitLink> getRefreshCommitLinks() {
        return refreshCommitLinks;
    }

    public void setRefreshCommitLinks(List<RefreshCommitLink> refreshCommitLinks) {
        this.refreshCommitLinks = refreshCommitLinks;
    }

    public List<RefreshCommitCoin> getRefreshCommitCoins() {
        return refreshCommitCoins;
    }

    public void setRefreshCommitCoins(List<RefreshCommitCoin> refreshCommitCoins) {
        this.refreshCommitCoins = refreshCommitCoins;
    }
}