/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa;

import com.ochafik.lang.jnaerator.runtime.NativeSize;
import com.sun.jna.Native;
import com.sun.jna.ptr.PointerByReference;

import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNetLibrary;
import org.gnunet.taler.wallet_android.utilities.JNAhelpers.MemoryUtils;

import java.math.BigInteger;
import java.nio.ByteBuffer;

/**
 * Helper class for RSA blinding keys
 *
 * @author Oliver Broome
 */
public class RSABlindingKey implements AutoCloseable {
    private GNUNetLibrary.GNUNET_CRYPTO_rsa_BlindingKey keyStruct;

    /**
     * Generates a new RSA blinding key with the given length
     *
     * @param keySize the length in bits the key should have
     */
    public RSABlindingKey(int keySize) {
        keyStruct = GNUNetLibrary.GNUNET_CRYPTO_rsa_blinding_key_create(keySize);
    }

    /**
     * Creates a blinding key from an existing BigInteger
     *
     * @param gnunetEncoded the BigInteger to decode
     */
    public RSABlindingKey(BigInteger gnunetEncoded) {
        byte[] keyBytes = gnunetEncoded.toByteArray();
        int keyBytesLength = keyBytes.length;

        ByteBuffer keyBuffer = ByteBuffer.allocateDirect(keyBytesLength).put(keyBytes);

        keyStruct = GNUNetLibrary.GNUNET_CRYPTO_rsa_blinding_key_decode(
                Native.getDirectBufferPointer(keyBuffer),
                new NativeSize(keyBytesLength));
    }

    /**
     * Creates a blinding key from a blob, useful for database operations
     *
     * @param blob the array of bytes representing the blinding key as encoded by
     *             GNUNET_CRYPTO_rsa_blinding_key_encode
     */
    public RSABlindingKey(byte[] blob) {
        this(new BigInteger(1, blob));
    }

    /**
     * Encodes the blinding key as a BigInteger
     * Make sure that the Integer is created as unsigned!
     *
     * @return the unsigned integer representation of the blinding key
     */
    public BigInteger encode() {
        if (keyStruct != null) {
            PointerByReference pointerByReference = new PointerByReference();

            NativeSize byteSize = GNUNetLibrary.GNUNET_CRYPTO_rsa_blinding_key_encode(keyStruct, pointerByReference);
            byte[] bytes = pointerByReference.getValue().getByteArray(0, byteSize.intValue());

            MemoryUtils.freePointerRef(pointerByReference, true);

            return new BigInteger(1, bytes); //the integer returned by the native function is unsigned, so we need this to be positive
        } else {
            throw new RuntimeException("No key in memory!");
        }
    }

    /**
     * Frees the memory associated with this blinding key
     */
    @Override
    public void close() {
        if (keyStruct != null) {
            GNUNetLibrary.GNUNET_CRYPTO_rsa_blinding_key_free(keyStruct);
            keyStruct = null;
        }
    }

    /**
     * Gets the structure representing the blinding key in memory
     *
     * @return the GNUNET_CRYPTO_rsa_BlindingKey Structure object
     */
    public GNUNetLibrary.GNUNET_CRYPTO_rsa_BlindingKey getStruct() {
        return keyStruct;
    }

    /**
     * Compares two blinding keys
     *
     * @param otherBlindingKey the RSABlindingKey to compare this one with
     * @return true, if the keys are equal
     */
    public boolean compare(RSABlindingKey otherBlindingKey) {
        return GNUNetLibrary.GNUNET_CRYPTO_rsa_blinding_key_cmp(
                this.getStruct(),
                otherBlindingKey.getStruct()) == 0;
    }
}
