/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.utilities;

import com.ochafik.lang.jnaerator.runtime.NativeSize;
import com.sun.jna.Memory;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.PointerUtils;

import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNET_CRYPTO_HashAsciiEncoded;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNET_HashCode;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNetLibrary;
import org.gnunet.taler.wallet_android.utilities.JNAhelpers.MemoryUtils;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.charset.StandardCharsets;

/**
 * Helper class for hashing (Exported hashes are Base32 (Crockford))
 *
 * @author Oliver Broome
 */
public class Hash implements AutoCloseable {
    private GNUNET_HashCode hashStruct;

    /**
     * Creates a new Hash from a String-type message
     *
     * @param message the message to be hashed
     */
    public Hash(String message) {
        byte[] messageBytes = message.getBytes(StandardCharsets.US_ASCII);
        final NativeSize messageSize = new NativeSize(messageBytes.length);

        Memory messageMemory = new Memory(messageSize.longValue());
        messageMemory.write(0, messageBytes, 0, messageBytes.length);

        hashStruct = new GNUNET_HashCode();

        GNUNetLibrary.GNUNET_CRYPTO_hash(
                messageMemory,
                new NativeSize(messageMemory.size()),
                hashStruct);

        MemoryUtils.freePointer(messageMemory);
    }

    /**
     * Copies a hash from memory.
     *
     * @param hashInMemory pointer to the hash data in memory
     */
    public Hash(Pointer hashInMemory) {
        IntBuffer ints = Native.getDirectByteBuffer(
                PointerUtils.getAddress(hashInMemory),
                GNUNetLibrary.GNUNET_CRYPTO_HASH_LENGTH
        ).asIntBuffer();
        int[] bits = new int[ints.capacity()];
        ints.get(bits);

        this.hashStruct = new GNUNET_HashCode(bits);
    }

    /**
     * Hashes arbitrary data in memory
     *
     * @param message the Memory object containing the data to be hashed
     */
    public Hash(Memory message) {
        this(message, new NativeSize(message.size()));
    }

    /**
     * Hashes arbitrary data in memory
     *
     * @param message Pointer to the data to be hashed
     * @param size    size of the message
     */
    public Hash(Pointer message, NativeSize size) {
        hashStruct = new GNUNET_HashCode();

        GNUNetLibrary.GNUNET_CRYPTO_hash(message, size, hashStruct);
    }

    /**
     * Hashes an array of bytes
     *
     * @param bytes the byte array to be hashed
     */
    public Hash(byte[] bytes) {
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(bytes.length).put(bytes);

        hashStruct = new GNUNET_HashCode.ByReference();

        Pointer bufferPointer = Native.getDirectBufferPointer(byteBuffer);
        GNUNetLibrary.GNUNET_CRYPTO_hash(
                bufferPointer,
                new NativeSize(byteBuffer.capacity()),
                hashStruct);

        MemoryUtils.freePointer(bufferPointer);
    }

    /**
     * Wraps an existing GNUNET_HashCode struct.
     *
     * @param hashStruct the GNUNET_HashCode structure to be wrapped
     */
    public Hash(GNUNET_HashCode hashStruct) {
        this.hashStruct = hashStruct;
    }

    /**
     * Get the underlying Structure object
     *
     * @return A GNUNET_HashCode struct
     */
    public GNUNET_HashCode getHashStruct() {
        return hashStruct;
    }

    /**
     * Returns a Crockford Base32 representation of the hash
     *
     * @return the encoded String
     */
    public String getAsciiEncoded() {
        if (hashStruct != null) {
            GNUNET_CRYPTO_HashAsciiEncoded encodedHashStruct = new GNUNET_CRYPTO_HashAsciiEncoded();

            GNUNetLibrary.GNUNET_CRYPTO_hash_to_enc(hashStruct, encodedHashStruct);

            return Native.toString(encodedHashStruct.encoding);
        } else {
            throw new RuntimeException("There is no hashStruct to decode!");
        }
    }

    /**
     * Returns the NativeSize for the Hash
     *
     * @return size_t of hashStruct.bits.length
     */
    public NativeSize getNativeSize() {
        return new NativeSize(hashStruct.bits.length * 4); //This correlates to that nasty hack I added in the GNUnet headers. (512 / 16 / 4; 4 used to be sizeof(uint32_t))
    }

    /**
     * Check another Hash for equality
     *
     * @param otherHash the Hash this Hash should be compared with
     * @return true, if the passed hash is equal
     */
    public boolean equals(Hash otherHash) {
        return GNUNetLibrary.GNUNET_CRYPTO_hash_cmp(hashStruct, otherHash.getHashStruct()) == 0;
    }

    /**
     * Remove the Hash from memory
     */
    @Override
    public void close() {
        if (hashStruct != null) {
            MemoryUtils.freeStruct(hashStruct);
            hashStruct = null;
        }
    }
}
