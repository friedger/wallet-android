/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.communication.mint.reserve;

import android.support.annotation.NonNull;

import com.google.gson.stream.JsonReader;

import org.gnunet.taler.wallet_android.communication.FunctionalEndpoint;
import org.gnunet.taler.wallet_android.communication.exceptions.UnknownJsonFieldException;
import org.gnunet.taler.wallet_android.database.model.objects.AmountDescriptor;
import org.gnunet.taler.wallet_android.database.model.objects.SepaDescriptor;
import org.gnunet.taler.wallet_android.database.model.state.DepositHistoryDescriptor;
import org.gnunet.taler.wallet_android.database.model.state.ReserveHistoryDescriptor;
import org.gnunet.taler.wallet_android.database.model.state.WithdrawalHistoryDescriptor;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSASignature;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Abstractions for /reserve REST API endpoints
 *
 * @author Oliver Broome
 */
public abstract class AbstractReserveEndpoint extends FunctionalEndpoint {
    /**
     * Initialises an abstract reserve endpoint
     *
     * @param baseURL the mint's base URL
     * @param target  the reserve endpoint method to target (currently "status" or "withdraw")
     * @param method  the HTTP method to use
     * @throws IOException
     */
    public AbstractReserveEndpoint(@NonNull URL baseURL, String target, @NonNull String method) throws IOException {
        super(baseURL, target, method);
    }

    /**
     * Parses a JSON-formatted reserve history
     *
     * @param reader the reader pointing to the history array
     * @return a list of Reserve history descriptors
     * @throws IOException
     */
    protected List<ReserveHistoryDescriptor> parseReserveHistory(JsonReader reader) throws IOException {
        List<ReserveHistoryDescriptor> result = new ArrayList<>();

        reader.beginArray();

        while (reader.hasNext()) {
            result.add(parseReserveHistoryItem(reader));
        }

        reader.endArray();

        return result;
    }

    /**
     * Parses a reserve history item
     *
     * @param reader the reader pointing to the history item
     * @return the parsed history item; can be a deposit or a withdraw descriptor
     * @throws IOException
     */
    private ReserveHistoryDescriptor parseReserveHistoryItem(JsonReader reader) throws IOException {
        ReserveHistoryDescriptor result;

        String type = null;
        EdDSASignature signature = null;
        AmountDescriptor amount = null;
        SepaDescriptor wireInformation = null;

        reader.beginObject();

        while (reader.hasNext()) {
            switch (reader.nextName()) {
                case "type":
                    type = reader.nextString();
                    break;
                case "amount":
                    amount = parseAmount(reader);
                    break;
                case "wire":
                    wireInformation = parseSepaDetails(reader);
                    break;
                case "signature":
                    signature = parseFullSignature(reader);
                    break;
                default:
                    throw new UnknownJsonFieldException(reader);
            }
        }

        if (wireInformation != null && "DEPOSIT".equals(type)) {
            result = new DepositHistoryDescriptor(amount, wireInformation);
        } else if (signature != null && "WITHDRAW".equals(type)) {
            result = new WithdrawalHistoryDescriptor(amount, signature);
        } else {
            throw new RuntimeException("Bad history item!");
        }

        reader.endObject();

        return result;
    }
}
