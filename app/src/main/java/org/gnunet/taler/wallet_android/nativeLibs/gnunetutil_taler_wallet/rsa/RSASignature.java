/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa;

import com.ochafik.lang.jnaerator.runtime.NativeSize;
import com.sun.jna.Memory;
import com.sun.jna.Native;
import com.sun.jna.ptr.PointerByReference;

import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNET_HashCode;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNetLibrary;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.utilities.Codec;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.utilities.Hash;
import org.gnunet.taler.wallet_android.utilities.JNAhelpers.MemoryUtils;

/**
 * Helper class for signing data with RSA
 *
 * @author Oliver Broome
 */
public class RSASignature implements AutoCloseable {
    private GNUNetLibrary.GNUNET_CRYPTO_rsa_Signature signatureStruct;

    /**
     * Wraps an existing RSA signature Structure from memory.
     *
     * @param signatureStruct the GNUNET_CRYPTO_rsa_Signature to wrap
     */
    public RSASignature(GNUNetLibrary.GNUNET_CRYPTO_rsa_Signature signatureStruct) {
        this.signatureStruct = signatureStruct;
    }

    /**
     * Duplicates an existing RSASignature in memory
     *
     * @param originalSignature the signature to copy the data from
     */
    public RSASignature(RSASignature originalSignature) {
        this.signatureStruct =
                GNUNetLibrary.GNUNET_CRYPTO_rsa_signature_dup(
                        originalSignature.getStruct());
    }

    /**
     * Signs an RSA-blinded message with the given private key.
     *
     * @param blindedMessage the blinded message
     * @param privateKey     the private key to be used for signing
     */
    public RSASignature(RSABlindedMessage blindedMessage, RSAPrivateKey privateKey) {
        this.signatureStruct = GNUNetLibrary.GNUNET_CRYPTO_rsa_sign(
                privateKey.getStruct(),
                blindedMessage.getPointer(),
                blindedMessage.getNativeSize());
    }

    /**
     * Creates a pre-built signature from an encoded S-expression.
     *
     * @param gnunetEncoded the String representing the signature
     */
    public RSASignature(String gnunetEncoded, boolean fromJSON) {
        if (fromJSON) {
            Memory decodedSExpression = Codec.decode(gnunetEncoded);
            signatureStruct = GNUNetLibrary.GNUNET_CRYPTO_rsa_signature_decode(
                    decodedSExpression,
                    new NativeSize(decodedSExpression.size()));
        } else {
            signatureStruct = GNUNetLibrary.GNUNET_CRYPTO_rsa_signature_decode(
                    gnunetEncoded,
                    new NativeSize(Native.toByteArray(gnunetEncoded).length));
        }
    }

    /**
     * Unblinds a blinded RSA signature.
     *
     * @param blindedSignature the blinded signature
     * @param blindingKey      the blinding key to use
     * @param publicKey        the public key of the signer
     */
    public RSASignature(RSASignature blindedSignature,
                        RSABlindingKey blindingKey,
                        RSAPublicKey publicKey) {
        this.signatureStruct = GNUNetLibrary.GNUNET_CRYPTO_rsa_unblind(
                blindedSignature.getStruct(),
                blindingKey.getStruct(),
                publicKey.getStruct());
    }

    /**
     * Signs the given hash with the given private key.
     *
     * @param messageHash the Hash to be signed
     * @param privateKey  the private RSA key to use
     */
    public RSASignature(Hash messageHash, RSAPrivateKey privateKey) {
        this.signatureStruct = GNUNetLibrary.GNUNET_CRYPTO_rsa_sign(
                privateKey.getStruct(),
                messageHash.getHashStruct().getPointer(),
                messageHash.getNativeSize());
    }

    /**
     * Verifies that the signature signs the given Hash object with the given public key.
     *
     * @param hash      the Hash to check against
     * @param publicKey the private key to check against
     * @return true, if the verification succeeds
     */
    public boolean verify(Hash hash, RSAPublicKey publicKey) {
        final GNUNetLibrary.GNUNET_CRYPTO_rsa_PublicKey publicKeyStruct = publicKey.getStruct();
        final GNUNET_HashCode hashStruct = hash.getHashStruct();

        if (hashStruct == null || publicKeyStruct == null || signatureStruct == null) {
            throw new RuntimeException("One or more necessary structs are not defined!");
        }

        int verificationResult = GNUNetLibrary.GNUNET_CRYPTO_rsa_verify(
                hashStruct,
                signatureStruct,
                publicKeyStruct);

        return verificationResult == GNUNetLibrary.GNUNET_OK;
    }

    /**
     * Encodes the signature with Base32(Crockford) encoding
     *
     * @return a Base32-encoded String
     */
    public String encode() {
        PointerByReference encodedStringPointerRef = new PointerByReference();
        NativeSize encodedSize = GNUNetLibrary.GNUNET_CRYPTO_rsa_signature_encode(
                signatureStruct,
                encodedStringPointerRef);

        String result = encodedStringPointerRef.getValue().getString(0);

        MemoryUtils.freePointerRef(encodedStringPointerRef, true);

        return result;
    }

    /**
     * Frees the memory allocated for this signature
     */
    @Override
    public void close() {
        if (signatureStruct != null) {
            GNUNetLibrary.GNUNET_CRYPTO_rsa_signature_free(signatureStruct);

            //System.gc();
        }
    }

    /**
     * Gets the signature's structure object.
     *
     * @return the GNUNET_CRYPTO_rsa_Signature object
     */
    public GNUNetLibrary.GNUNET_CRYPTO_rsa_Signature getStruct() {
        return signatureStruct;
    }

    /**
     * Compares this signature with another.
     *
     * @param otherSignature the signature to compare this signature with
     * @return true, if both signatures have the same content
     */
    public boolean compare(RSASignature otherSignature) {
        return GNUNetLibrary.GNUNET_CRYPTO_rsa_signature_cmp(
                signatureStruct,
                otherSignature.getStruct()) == 0;
    }
}
