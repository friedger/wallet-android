/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.talerutil_wallet.jna;

import com.sun.jna.Pointer;
import com.sun.jna.Structure;

import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNetLibrary.GNUNET_CRYPTO_rsa_BlindingKey;

import java.util.Collections;
import java.util.List;

/**
 * @brief Type of blinding keys for Taler.<br>
 * This file was autogenerated by <a href="http://jnaerator.googlecode.com/">JNAerator</a>,<br>
 * a tool written by <a href="http://ochafik.com/">Olivier Chafik</a> that <a href="http://code.google.com/p/jnaerator/wiki/CreditsAndLicense">uses a few opensource projects.</a>.<br>
 * For help, please visit <a href="http://nativelibs4java.googlecode.com/">NativeLibs4Java</a> , <a href="http://rococoa.dev.java.net/">Rococoa</a>, or <a href="http://jna.dev.java.net/">JNA</a>.
 */
public class TALER_DenominationBlindingKey extends Structure {
    /**
     * C type : GNUNET_CRYPTO_rsa_BlindingKey*
     */
    public GNUNET_CRYPTO_rsa_BlindingKey rsa_blinding_key;

    public TALER_DenominationBlindingKey() {
        super();
    }

    /**
     * @param rsa_blinding_key C type : GNUNET_CRYPTO_rsa_BlindingKey*
     */
    public TALER_DenominationBlindingKey(GNUNET_CRYPTO_rsa_BlindingKey rsa_blinding_key) {
        super();
        this.rsa_blinding_key = rsa_blinding_key;
    }

    public TALER_DenominationBlindingKey(Pointer peer) {
        super(peer);
    }

    protected List<?> getFieldOrder() {
        return Collections.singletonList("rsa_blinding_key");
    }

    public static class ByReference extends TALER_DenominationBlindingKey implements Structure.ByReference {

    }

    public static class ByValue extends TALER_DenominationBlindingKey implements Structure.ByValue {

    }
}
