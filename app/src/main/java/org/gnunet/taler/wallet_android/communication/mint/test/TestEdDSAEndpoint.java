/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.communication.mint.test;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import org.gnunet.taler.wallet_android.communication.CommunicationConstants;
import org.gnunet.taler.wallet_android.communication.Endpoint;
import org.gnunet.taler.wallet_android.communication.exceptions.UnknownJsonFieldException;
import org.gnunet.taler.wallet_android.communication.mint.MintEndpointConstants;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.ECCSignaturePurpose;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSAPublicKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSASignature;
import org.gnunet.taler.wallet_android.nativeLibs.talerutil_wallet.jna.TalerLibrary;

import java.io.IOException;
import java.net.URL;

/**
 * Represents the /test/eddsa endpoint of the REST API
 *
 * @author Oliver Broome
 */
public class TestEdDSAEndpoint extends Endpoint {
    private EdDSAPublicKey receivedPublicKey;
    private EdDSASignature receivedSignature;
    private ECCSignaturePurpose receivedSignaturePurpose;

    public TestEdDSAEndpoint(@NonNull URL baseURL, EdDSAPublicKey publicKey, EdDSASignature signature) throws IOException {
        super(baseURL, MintEndpointConstants.TEST_PREFIX + "/eddsa", CommunicationConstants.METHOD_POST);

        PostData postData = new PostData(publicKey.encode(), signature.encode());

        Gson gson = new Gson();
        String jsonData = gson.toJson(postData);

        prepareRequestBody(jsonData);
    }

    public ECCSignaturePurpose getReceivedSignaturePurpose() {
        return receivedSignaturePurpose;
    }

    @Override
    public void parseResults(@NonNull JsonReader reader) throws IOException {
        reader.beginObject();

        while (reader.hasNext()) {
            switch (reader.nextName()) {
                case "eddsa_pub":
                    receivedPublicKey = new EdDSAPublicKey(reader.nextString());
                    break;
                case "eddsa_sig":
                    receivedSignaturePurpose = new ECCSignaturePurpose(
                            null, 0,
                            TalerLibrary.Taler_Signatures.TALER_SIGNATURE_MINT_TEST_EDDSA);
                    receivedSignature = new EdDSASignature(
                            receivedSignaturePurpose,
                            reader.nextString());
                    break;
                default:
                    throw new UnknownJsonFieldException(reader);
            }
        }

        reader.endObject();

        reader.close();
    }

    public EdDSAPublicKey getReceivedPublicKey() {
        return receivedPublicKey;
    }

    public EdDSASignature getReceivedSignature() {
        return receivedSignature;
    }

    private class PostData {
        private String eddsa_pub;
        private String eddsa_sig;

        private PostData(String eddsa_pub, String eddsa_sig) {
            this.eddsa_pub = eddsa_pub;
            this.eddsa_sig = eddsa_sig;
        }
    }
}
