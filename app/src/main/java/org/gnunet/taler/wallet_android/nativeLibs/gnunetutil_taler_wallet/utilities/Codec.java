/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.utilities;

import com.ochafik.lang.jnaerator.runtime.NativeSize;
import com.sun.jna.Memory;
import com.sun.jna.Pointer;
import com.sun.jna.Structure;

import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNetLibrary;
import org.gnunet.taler.wallet_android.utilities.JNAhelpers.MemoryUtils;

import java.nio.charset.StandardCharsets;

/**
 * Helper class for GNUnet's string functions
 *
 * @author Oliver Broome
 */
public class Codec {
    /**
     * Encodes data in memory to Base32 (Crockford)
     *
     * @param structure the Structure the
     * @return the Base32(Crockford)-encoded String
     * @see GNUNetLibrary#GNUNET_STRINGS_data_to_string_alloc(Pointer, NativeSize)
     */
    public static String encode(Structure structure) {
        Pointer buffer = GNUNetLibrary.
                GNUNET_STRINGS_data_to_string_alloc(
                        structure.getPointer(),
                        new NativeSize(structure.size()));
        String result = buffer.getString(0, "ASCII");

        MemoryUtils.freePointer(buffer);

        return result;
    }

    /**
     * Encodes data in memory to Base32 (Crockford)
     *
     * @param data the Memory object to be used
     * @return the Base32(Crockford)-encoded String
     * @see GNUNetLibrary#GNUNET_STRINGS_data_to_string_alloc(Pointer, NativeSize)
     */
    public static String encode(Memory data) {
        return encode(data, new NativeSize(data.size()));
    }

    /**
     * Encodes data in memory to Base32 (Crockford)
     *
     * @param data Pointer to the data in memory
     * @param size length of the data to be encoded
     * @return the Base32(Crockford)-encoded String
     * @see GNUNetLibrary#GNUNET_STRINGS_data_to_string_alloc(Pointer, NativeSize)
     */
    public static String encode(Pointer data, NativeSize size) {
        Pointer buffer = GNUNetLibrary.GNUNET_STRINGS_data_to_string_alloc(data, size);
        String result = buffer.getString(0, "ASCII");

        MemoryUtils.freePointer(buffer);

        return result;
    }

    /**
     * Decodes a Base32(Crockford)-encoded String
     *
     * @param encoded the String to be decoded
     * @return a Memory object holding the decoded data, should be freed after use.
     */
    public static Memory decode(String encoded) {
        long encodedByteCount = encoded.getBytes(StandardCharsets.US_ASCII).length; //GNUNET_STRINGS_string_to_data does not require a null-terminator, so no Native.toByteArray
        long bytesToAllocate = (encodedByteCount * 5) / 8;

        Memory outputMemory = new Memory(bytesToAllocate);

        if (GNUNetLibrary.GNUNET_OK ==
                GNUNetLibrary.GNUNET_STRINGS_string_to_data(
                        encoded,
                        new NativeSize(encodedByteCount),
                        outputMemory,
                        new NativeSize(outputMemory.size()))) {
            return outputMemory;
        } else {
            throw new RuntimeException("Could not decode the string!");
        }
    }
}
