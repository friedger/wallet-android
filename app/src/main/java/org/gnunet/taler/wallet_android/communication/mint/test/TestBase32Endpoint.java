/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.communication.mint.test;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.sun.jna.Memory;

import org.gnunet.taler.wallet_android.communication.CommunicationConstants;
import org.gnunet.taler.wallet_android.communication.Endpoint;
import org.gnunet.taler.wallet_android.communication.exceptions.UnknownJsonFieldException;
import org.gnunet.taler.wallet_android.communication.mint.MintEndpointConstants;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.utilities.Codec;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.utilities.Hash;

import java.io.IOException;
import java.net.URL;

/**
 * Tests the functionality of the hashing and Base32 encoding functions via a mint's test API
 *
 * @author Oliver Broome
 */
public class TestBase32Endpoint extends Endpoint {
    private final String expectedResponse;
    private String actualResponse;

    public TestBase32Endpoint(@NonNull URL baseURL, Memory message) throws IOException {
        super(baseURL, MintEndpointConstants.TEST_PREFIX + "/base32", CommunicationConstants.METHOD_POST);

        try (Hash hashedMessage = new Hash(message)) {
            expectedResponse = hashedMessage.getAsciiEncoded();
        }

        PostJson postBody = new PostJson(Codec.encode(message));
        Gson gson = new Gson();

        String postData = gson.toJson(postBody);

        prepareRequestBody(postData);
    }

    public String getActualResponse() {
        return actualResponse;
    }

    public String getExpectedResponse() {
        return expectedResponse;
    }

    @Override
    public void parseResults(@NonNull JsonReader reader) throws IOException {
        reader.beginObject();

        if ("output".equals(reader.nextName())) {
            actualResponse = reader.nextString();
        } else {
            throw new UnknownJsonFieldException(reader);
        }

        reader.endObject();

        reader.close();
    }

    @Override
    protected void handleErrors(int responseCode, @NonNull JsonReader reader) throws IOException {
        handleDefaultError(responseCode, reader);
    }


    private class PostJson {
        private String input;

        public PostJson(String input) {
            this.input = input;
        }
    }
}
