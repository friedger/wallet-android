/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.communication.exceptions;

import android.util.Log;

import com.google.gson.stream.JsonReader;

import java.io.IOException;

/**
 * Exception for unknown/non-existent reserves
 *
 * @author Oliver Broome
 */
public class ReserveNotFoundException extends IOException {
    public ReserveNotFoundException(String denominationPublicKey, String reservePublicKey, JsonReader reader) {
        super(prepareMessage(denominationPublicKey, reservePublicKey, reader));
    }

    public ReserveNotFoundException(String reservePublicKey) {
        super(reservePublicKey);
    }

    private static String prepareMessage(String denominationPublicKey, String reservePublicKey, JsonReader reader) {
        String result = null;

        try {
            while (reader.hasNext()) {
                switch (reader.nextString()) {
                    case "error":
                        //empty, skip.
                        break;
                    case "parameter":
                        // this is the queried reserve's public key
                        result = reader.nextString();
                        break;
                    default:
                        throw new UnknownJsonFieldException(reader);
                }
            }

            if (result != null) {
                switch (result) {
                    case "denom_pub":
                        throw new InvalidDenominationException(denominationPublicKey);
                    case "reserve_pub":
                        throw new ReserveNotFoundException(reservePublicKey);
                    default:
                        //not much to do here, fail silently
                        break;
                }
            }

            reader.close();
        } catch (IOException e) {
            Log.w("taler_json_parser", "The JsonReader could not be closed!", e);
        }

        return result;
    }
}
