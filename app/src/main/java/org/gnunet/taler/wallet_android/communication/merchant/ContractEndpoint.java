/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.communication.merchant;

import android.support.annotation.NonNull;

import com.google.gson.stream.JsonReader;

import org.gnunet.taler.wallet_android.communication.FunctionalEndpoint;
import org.gnunet.taler.wallet_android.communication.exceptions.UnknownJsonFieldException;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.ECCSignaturePurpose;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSAPublicKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSASignature;
import org.gnunet.taler.wallet_android.nativeLibs.talerutil_wallet.Contract;
import org.gnunet.taler.wallet_android.nativeLibs.talerutil_wallet.jna.TalerLibrary;

import java.io.IOException;
import java.net.URL;

import static org.gnunet.taler.wallet_android.communication.CommunicationConstants.METHOD_GET;
import static org.gnunet.taler.wallet_android.communication.merchant.MerchantEndpointConstants.MERCHANT_PREFIX;

/**
 * Represents the /taler/contract endpoint of the mint API.
 *
 * @author Oliver Broome
 */
public class ContractEndpoint extends FunctionalEndpoint {
    private Contract contract;
    private EdDSASignature contractSignature;
    private EdDSAPublicKey merchantPublicKey;

    /**
     * Initialises a Contract endpoint
     *
     * @param baseURL the merchant's base URL
     * @throws IOException
     */
    public ContractEndpoint(@NonNull URL baseURL) throws IOException {
        super(baseURL, MERCHANT_PREFIX + "/contract", METHOD_GET);
    }

    @Override
    public void parseResults(@NonNull JsonReader reader) throws IOException {
        reader.beginObject();

        while (reader.hasNext()) {
            switch (reader.nextName()) {
                case "contract":
                    contract = new Contract(reader.nextString());
                    break;
                case "sig":
                    contractSignature = new EdDSASignature(null, reader.nextString());
                    break;
                case "eddsa_pub":
                    merchantPublicKey = new EdDSAPublicKey(reader.nextString());
                    break;
                default:
                    throw new UnknownJsonFieldException(reader);
            }
        }

        reader.endObject();

        reader.close();
    }

    /**
     * Gets the contract descriptor. Remember to call connect() first!
     *
     * @return the contract offered by the merchant
     */
    public Contract getContract() {
        return contract;
    }

    @Override
    public void close() {
        super.close();

        if (contractSignature != null) {
            contractSignature.close();
            contractSignature = null;
        }

        if (merchantPublicKey != null) {
            merchantPublicKey.close();
            merchantPublicKey = null;
        }
    }

    @Override
    public boolean verify() {
        try (ECCSignaturePurpose purpose = new ECCSignaturePurpose(null, 0, TalerLibrary.Taler_Signatures.TALER_SIGNATURE_MERCHANT_CONTRACT)) {
            contractSignature.setSignaturePurposeStruct(purpose.getStruct());
            return contractSignature.verify(purpose, merchantPublicKey);
        }
    }
}
