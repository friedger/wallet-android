/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.database.storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import org.gnunet.taler.wallet_android.database.model.infrastructure.MintSigningKeyDescriptor;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSAPublicKey;

import java.util.Calendar;

import static org.gnunet.taler.wallet_android.database.storage.DBConstants.AUTOINCREMENT;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.BEGIN_COLUMNS;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.BLOB;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.CASCADE;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.CHECK;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.END_COLUMNS;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.INTEGER;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.NEXT_COLUMN;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.NOT_NULL;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.ON_DELETE;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.PRIMARY_KEY;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.REFERENCES;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.WHERE;

/**
 * Allows access to the mint signing key table
 *
 * @author Oliver Broome
 */
public class MintSigningKeysDatabase extends AbstractDatabase {
    public static final String TABLE_NAME = "mint_signkeys";

    public static final String COLUMN_MINT_ID = "mintid";
    public static final String COLUMN_SIGN_PUB = "sign_pub";
    public static final String COLUMN_EXPIRE = "expire";

    public static final String TABLE_CREATE =
            "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + BEGIN_COLUMNS +
                    BaseColumns._ID + INTEGER + PRIMARY_KEY + AUTOINCREMENT + NEXT_COLUMN +
                    COLUMN_MINT_ID + INTEGER + REFERENCES + MintsDatabase.TABLE_NAME + " (" + BaseColumns._ID + ")" + ON_DELETE + CASCADE + NEXT_COLUMN +
                    COLUMN_SIGN_PUB + BLOB + NOT_NULL + CHECK + "(length(" + COLUMN_SIGN_PUB + ") = 32)" + NEXT_COLUMN +
                    COLUMN_EXPIRE + INTEGER +
                    END_COLUMNS;

    private static final String MINT_ID_WHERE = WHERE + COLUMN_MINT_ID + " = ?";

    private static final String[] MINT_KEY_PROJECTION = new String[]{
            COLUMN_SIGN_PUB, COLUMN_EXPIRE
    };

    /**
     * WHERE clause for all signing keys more than a day over their expiry date
     */
    private static final String EXPIRED_KEYS_WHERE =
            WHERE + COLUMN_EXPIRE + " < " + Long.toString(System.currentTimeMillis() + 86400000);

    public MintSigningKeysDatabase(Context context, SQLiteOpenHelper databaseHelper) {
        super(context, databaseHelper);
    }

    public long addSigningKey(long mintId, EdDSAPublicKey signingKey, Calendar expiryDate) {
        try (SQLiteDatabase database = databaseHelper.getWritableDatabase()) {
            ContentValues values = new ContentValues(3);

            values.put(COLUMN_MINT_ID, mintId);
            values.put(COLUMN_SIGN_PUB, signingKey.getStruct().q_y);
            values.put(COLUMN_EXPIRE, expiryDate.getTimeInMillis());

            return database.insert(TABLE_NAME, null, values);
        }
    }

    public Cursor getMintSigningKeys(long mintId, int limit) {
        try (SQLiteDatabase database = databaseHelper.getReadableDatabase()) {
            return database.query(
                    TABLE_NAME,
                    MINT_KEY_PROJECTION,
                    MINT_ID_WHERE,
                    new String[]{Long.toString(mintId)},
                    null, null,
                    Integer.toString(limit));
        }
    }

    public int removeOldSigningKeys() {
        try (SQLiteDatabase database = databaseHelper.getWritableDatabase()) {
            return database.delete(TABLE_NAME, EXPIRED_KEYS_WHERE, null);
        }
    }

    private class Reader extends DatabaseReader {
        public Reader(Cursor cursor) {
            super(cursor);
        }

        @Override
        public MintSigningKeyDescriptor getCurrent() {
            EdDSAPublicKey publicSigningKey =
                    new EdDSAPublicKey(
                            cursor.getBlob(cursor.getColumnIndexOrThrow(COLUMN_SIGN_PUB)));

            Calendar expiry = Calendar.getInstance();
            expiry.setTimeInMillis(cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_EXPIRE)));

            return new MintSigningKeyDescriptor(publicSigningKey, expiry);
        }
    }
}
