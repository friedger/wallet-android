/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.database.model.infrastructure;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.gnunet.taler.wallet_android.database.model.Descriptor;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSAPublicKey;

import java.net.URL;

/**
 * Represents a mint
 *
 * @author Oliver Broome
 */
public class MintDescriptor implements Descriptor, AutoCloseable {
    /**
     * The mint's ID in the database
     */

    private long id;
    /**
     * The mint's official name
     */

    private String name;
    /**
     * The mint's URL
     */
    private URL url;

    /**
     * The mint's main public key
     */
    private EdDSAPublicKey publicKey;

    /**
     * Creates a new mint descriptor
     *
     * @param id        the mint's database row id, set to -1 if unknown
     * @param name      the mint's name
     * @param url       the mint's URL
     * @param publicKey the mint's public key, set to null if unknown
     */
    public MintDescriptor(long id, @NonNull String name, @NonNull URL url, @Nullable EdDSAPublicKey publicKey) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.publicKey = publicKey;
    }

    public EdDSAPublicKey getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(EdDSAPublicKey publicKey) {
        this.publicKey = publicKey;
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getMintID() {
        return id;
    }

    public void setMintID(long id) {
        this.id = id;
    }

    @Override
    public void close() {
        if (publicKey != null) {
            publicKey.close();
            publicKey = null;
        }
    }
}
