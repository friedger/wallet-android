/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.sun.jna.Memory;
import com.sun.jna.Native;
import com.sun.jna.Pointer;

import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNET_CRYPTO_EccSignaturePurpose;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.utilities.Codec;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.utilities.Hash;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.utilities.HashContext;
import org.gnunet.taler.wallet_android.utilities.JNAhelpers.MemoryUtils;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

/**
 * Helper class for ECC signature purposes <br /><br />
 * Layout: <br />
 * - size (Length of size+purpose+data (4+4+x bytes) ) <br />
 * - purpose (GNUNET_SIGNATURE_Purpose value) <br />
 * - data
 *
 * @author Oliver Broome
 * @see org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNET_CRYPTO_EccSignaturePurpose
 */
public class ECCSignaturePurpose implements AutoCloseable {
    private Memory signaturePurposeData;
    private GNUNET_CRYPTO_EccSignaturePurpose purposeStruct;

    /**
     * Decodes a signature purpose from a Base32-encoded String
     *
     * @param gnunetEncoded the encoded signature purpose
     */
    public ECCSignaturePurpose(@NonNull String gnunetEncoded) {
        signaturePurposeData = Codec.decode(gnunetEncoded);
        purposeStruct = new GNUNET_CRYPTO_EccSignaturePurpose(signaturePurposeData);
        purposeStruct.read();
    }

    /**
     * Copies the contents of a String "behind" a GNUNET_CRYPTO_EccSignaturePurpose <br />
     * Note that the data will be hashed hashed internally by the signature function.
     *
     * @param data           the String containing the data to be copied
     * @param nullTerminated whether the resulting purpose data should have a null terminator
     * @param purpose        the intended purpose for the signature
     */
    public ECCSignaturePurpose(String data, boolean nullTerminated, int purpose) {
        byte[] stringBytes;

        if (nullTerminated) {
            stringBytes = Native.toByteArray(data, "ASCII");
        } else {
            stringBytes = data.getBytes(StandardCharsets.US_ASCII);
        }

        ByteBuffer directBuffer = ByteBuffer.allocateDirect(stringBytes.length);
        directBuffer.put(stringBytes);

        prepare(Native.getDirectBufferPointer(directBuffer), directBuffer.capacity(), purpose);
    }

    /**
     * Copies the contents of the given String array "behind" a GNUNET_CRYPTO_EccSignaturePurpose <br />
     * Note that the data will be hashed hashed internally by the signature function.
     *
     * @param data           the Strings containing the data to be copied
     * @param nullTerminated whether the Strings in the resulting purpose data should have a null terminator
     * @param purpose        the intended purpose for the signature
     */
    public ECCSignaturePurpose(String[] data, boolean nullTerminated, int purpose) {
        HashContext hashContext = new HashContext();

        for (String item : data) {
            hashContext.add(item, nullTerminated);
        }

        Hash result = hashContext.finish();

        prepare(result.getHashStruct().getPointer(), result.getNativeSize().intValue(), purpose);
    }

    /**
     * Copies the contents of a direct ByteBuffer "behind" a GNUNET_CRYPTO_EccSignaturePurpose <br />
     * Note that the data will be hashed hashed internally by the signature function.
     *
     * @param data    the direct ByteBuffer containing the data to be copied
     * @param purpose the intended purpose for the signature
     * @throws IllegalStateException
     */
    public ECCSignaturePurpose(ByteBuffer data, int purpose) throws IllegalStateException {
        if (!data.isDirect()) {
            throw new IllegalStateException("The ByteBuffer passed to this constructor must be a direct buffer!");
        }

        prepare(Native.getDirectBufferPointer(data), data.capacity(), purpose);
    }

    /**
     * Copies data from another point in memory "behind" a GNUNET_CRYPTO_EccSignaturePurpose <br />
     * Note that the data will be hashed hashed internally by the signature function.
     *
     * @param data       a Pointer to the data in memory
     * @param dataLength the length of the data
     * @param purpose    the intended purpose for the signature
     */
    public ECCSignaturePurpose(@Nullable Pointer data, int dataLength, int purpose) {
        prepare(data, dataLength, purpose);
    }

    /**
     * Prepares the ECCSignaturePurpose for consumption
     *
     * @param data       Pointer to the data to be copied
     * @param dataLength length of the data
     * @param purpose    purpose of the data for the signature
     */
    private void prepare(@Nullable Pointer data, int dataLength, int purpose) {
        signaturePurposeData = new Memory(8 + dataLength);

        purposeStruct = new GNUNET_CRYPTO_EccSignaturePurpose(signaturePurposeData);
        purposeStruct.purpose = Integer.reverseBytes(purpose);      //network byte order!
        purposeStruct.size = Integer.reverseBytes(dataLength + 8);

        if (data != null && dataLength > 0) {
            signaturePurposeData.write(8, data.getByteArray(0, dataLength), 0, dataLength);
        }
    }

    /**
     * De-allocates the memory reserved during the construction of the object and removes references
     * to the underlying structure objects
     */
    @Override
    public void close() {
        if (signaturePurposeData != null) {
            MemoryUtils.freePointer(signaturePurposeData);
            signaturePurposeData = null;
            purposeStruct = null;
        }
    }

    /**
     * Gets the amount of memory allocated for this object
     *
     * @return the amount of memory reserved for the signature data and the purpose structure
     */
    public long getSize() {
        return signaturePurposeData.size();
    }

    /**
     * Gets the purpose code of the included GNUNET_CRYPTO_EccSignaturePurpose structure
     *
     * @return the purpose code
     * @see org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNET_CRYPTO_EccSignaturePurpose
     */
    public int getPurposeCode() {
        return Integer.reverseBytes(purposeStruct.purpose);
    }

    /**
     * Gets a pointer to the actual data contained in the allocated memory
     *
     * @return a Pointer to the location of the GNUNET_CRYPTO_EccSignaturePurpose + 8
     */
    public Pointer getDataPointer() {
        return signaturePurposeData.share(8);
    }

    /**
     * Gets the location of the purpose structure in memory
     *
     * @return a GNUNET_CRYPTO_EccSignaturePurpose JNA Structure object
     */
    public GNUNET_CRYPTO_EccSignaturePurpose getStruct() {
        return purposeStruct;
    }
}