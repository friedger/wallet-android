/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.communication.merchant;

import android.support.annotation.NonNull;

import com.google.gson.stream.JsonReader;

import org.gnunet.taler.wallet_android.communication.Endpoint;
import org.gnunet.taler.wallet_android.communication.exceptions.UnknownJsonFieldException;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSAPublicKey;

import java.io.IOException;
import java.net.URL;

import static org.gnunet.taler.wallet_android.communication.CommunicationConstants.METHOD_GET;
import static org.gnunet.taler.wallet_android.communication.merchant.MerchantEndpointConstants.MERCHANT_PREFIX;

/**
 * Represents the /taler/key endpoint of the merchant API.
 *
 * @author Oliver Broome
 */
public class KeyEndpoint extends Endpoint {
    private EdDSAPublicKey publicKey;

    /**
     * Initialises a new Key endpoint for retrieving a merchant's public key
     *
     * @param baseURL the merchant's base URL
     * @throws IOException
     */
    public KeyEndpoint(@NonNull URL baseURL) throws IOException {
        super(baseURL, MERCHANT_PREFIX + "/key", METHOD_GET);
    }

    /**
     * Gets the merchants public EdDSA public key. Remember to call connect() first!
     *
     * @return the public key
     */
    public EdDSAPublicKey getPublicKey() {
        return publicKey;
    }

    @Override
    protected void parseResults(@NonNull JsonReader reader) throws IOException {
        reader.beginObject();

        if ("merchant_pub".equals(reader.nextName())) {
            publicKey = new EdDSAPublicKey(reader.nextString());
        } else {
            throw new UnknownJsonFieldException(reader);
        }

        reader.endObject();

        reader.close();
    }
}
