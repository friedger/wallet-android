/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gcrypt.jna;

import com.sun.jna.Pointer;
import com.sun.jna.Structure;
import com.sun.jna.Union;

import java.util.Arrays;
import java.util.List;

/**
 * This file was autogenerated by <a href="http://jnaerator.googlecode.com/">JNAerator</a>,<br>
 * a tool written by <a href="http://ochafik.com/">Olivier Chafik</a> that <a href="http://code.google.com/p/jnaerator/wiki/CreditsAndLicense">uses a few opensource projects.</a>.<br>
 * For help, please visit <a href="http://nativelibs4java.googlecode.com/">NativeLibs4Java</a> , <a href="http://rococoa.dev.java.net/">Rococoa</a>, or <a href="http://jna.dev.java.net/">JNA</a>.
 */
public class _gpgrt_syshd extends Structure {
    /**
     * @see gpgrt_syshd_types<br>
     * C type : gpgrt_syshd_types
     */
    public int type;
    /**
     * C type : u_union
     */
    public u_union u;

    public _gpgrt_syshd() {
        super();
    }

    /**
     * @param type @see gpgrt_syshd_types<br>
     *             C type : gpgrt_syshd_types<br>
     * @param u    C type : u_union
     */
    public _gpgrt_syshd(int type, u_union u) {
        super();
        this.type = type;
        this.u = u;
    }

    public _gpgrt_syshd(Pointer peer) {
        super(peer);
    }

    protected List<?> getFieldOrder() {
        return Arrays.asList("type", "u");
    }

    public static class u_union extends Union {
        public int fd;
        public int sock;
        public int rvid;
        /**
         * C type : void*
         */
        public Pointer handle;

        public u_union() {
            super();
        }

        public u_union(int fd_or_sock_or_rvid) {
            super();
            this.rvid = this.sock = this.fd = fd_or_sock_or_rvid;
            setType(Integer.TYPE);
        }

        /**
         * @param handle C type : void*
         */
        public u_union(Pointer handle) {
            super();
            this.handle = handle;
            setType(Pointer.class);
        }

        public static class ByReference extends u_union implements Structure.ByReference {

        }

        public static class ByValue extends u_union implements Structure.ByValue {

        }
    }

    public static class ByReference extends _gpgrt_syshd implements Structure.ByReference {

    }

    public static class ByValue extends _gpgrt_syshd implements Structure.ByValue {

    }
}
