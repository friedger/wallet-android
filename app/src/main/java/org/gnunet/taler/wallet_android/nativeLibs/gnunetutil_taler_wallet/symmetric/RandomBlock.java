/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.symmetric;

import com.ochafik.lang.jnaerator.runtime.NativeSize;
import com.sun.jna.Memory;

import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNetLibrary;
import org.gnunet.taler.wallet_android.utilities.JNAhelpers.MemoryUtils;

/**
 * Helper class for getting random data from GNUNET_CRYPTO_random_block
 *
 * @author Oliver Broome
 */
public class RandomBlock implements AutoCloseable {
    public final static int SALT_BLOCK = 32;
    public final static int KEY_BLOCK = 64;
    protected Memory block;

    /**
     * Creates a randomised block of memory with 'strong' cryptographic properties
     *
     * @param size the size of the block in bytes
     * @see GNUNetLibrary.GNUNET_CRYPTO_Quality
     */
    public RandomBlock(int size) {
        block = new Memory(size);
        GNUNetLibrary.GNUNET_CRYPTO_random_block(
                GNUNetLibrary.GNUNET_CRYPTO_Quality.GNUNET_CRYPTO_QUALITY_STRONG,
                block,
                new NativeSize(block.size()));
    }

    /**
     * Creates a randomised block of memory with the specified cryptographic quality
     *
     * @param size     the size of the block in bytes
     * @param strength the desired cryptographic quality
     * @see GNUNetLibrary.GNUNET_CRYPTO_Quality
     */
    public RandomBlock(int size, int strength) {
        block = new Memory(size);
        GNUNetLibrary.GNUNET_CRYPTO_random_block(
                strength,
                block,
                new NativeSize(block.size()));
    }

    public RandomBlock() {
    }

    /**
     * Formats the size of the random block as a NativeSize
     *
     * @return the NativeSize
     */
    public final NativeSize getNativeSize() {
        return new NativeSize(block.size());
    }

    public final Memory getPointer() {
        return block;
    }

    @Override
    public void close() {
        if (block != null) {
            block.clear();
            MemoryUtils.freePointer(block);
            block = null;
        }
    }
}
