/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.utilities.JNAhelpers;

import com.ochafik.lang.jnaerator.runtime.NativeSize;
import com.sun.jna.Native;
import com.sun.jna.Pointer;

import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 * Utility function for converting Java Strings to native representations
 *
 * @author Oliver Broome
 */
public class NativeString implements AutoCloseable {
    private Pointer nativeString;
    private NativeSize nativeSize;

    public NativeString(String string) {
        this(string, true);
    }

    public NativeString(String string, String encoding) {
        this(string, true, encoding);
    }

    public NativeString(String string, boolean nullTerminated) {
        this(string, nullTerminated, "ASCII");
    }

    public NativeString(String string, boolean nullTerminated, String encoding) {
        byte[] stringBytes;
        if (nullTerminated) {
            stringBytes = Native.toByteArray(string, encoding);
        } else {
            byte[] tmp = Native.toByteArray(encoding);
            stringBytes = Arrays.copyOf(tmp, tmp.length - 1);
        }

        ByteBuffer stringBuffer = ByteBuffer.allocateDirect(stringBytes.length);
        stringBuffer.put(stringBytes);

        nativeSize = new NativeSize(stringBuffer.capacity());
        nativeString = Native.getDirectBufferPointer(stringBuffer);
    }

    public Pointer getPointer() {
        return nativeString;
    }

    @Override
    public void close() {
        if (nativeString != null) {
            MemoryUtils.freePointer(nativeString);
            nativeString = null;
        }
    }

    public NativeSize getNativeSize() {
        return nativeSize;
    }
}
