/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.ui;

/**
 * Constants for the Intent fields
 *
 * @author Oliver Broome
 */
public interface IntentConstants {
    String EXTRA_MINT_ID = "org.gnunet.taler.wallet_android.EXTRA_MINT_ID";
    String EXTRA_MINT_NAME = "org.gnunet.taler.wallet_android.EXTRA_MINT_NAME";

    String EXTRA_RESERVE_ID = "org.gnunet.taler.wallet_android.EXTRA_RESERVE_ID";
    String EXTRA_RESERVE_PUBLIC_KEY = "org.gnunet.taler.wallet_android.EXTRA_RESERVE_PUBLIC_KEY";

    String EXTRA_RESERVE_CURRENCY = "org.gnunet.taler.wallet_android.EXTRA_RESERVE_CURRENCY";
    String EXTRA_RESERVE_VALUE = "org.gnunet.taler.wallet_android.EXTRA_RESERVE_VALUE";
    String EXTRA_RESERVE_FRACTION = "org.gnunet.taler.wallet_android.EXTRA_RESERVE_FRACTION";
}
