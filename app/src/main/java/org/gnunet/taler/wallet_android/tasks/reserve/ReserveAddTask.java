/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.tasks.reserve;

import android.os.AsyncTask;

import org.gnunet.taler.wallet_android.database.DatabaseFactory;
import org.gnunet.taler.wallet_android.database.model.objects.ReserveDescriptor;
import org.gnunet.taler.wallet_android.database.storage.ReserveDatabase;
import org.gnunet.taler.wallet_android.ui.fragments.ReserveGeneratorWorkerFragment;

import java.lang.ref.WeakReference;

/**
 * Task for adding a reserve to the database
 *
 * @author Oliver Broome
 */
public class ReserveAddTask extends AsyncTask<ReserveDescriptor, Integer, ReserveDescriptor[]> {
    private WeakReference<ReserveGeneratorWorkerFragment> caller;

    public ReserveAddTask(WeakReference<ReserveGeneratorWorkerFragment> caller) {
        this.caller = caller;
    }

    @Override
    protected ReserveDescriptor[] doInBackground(ReserveDescriptor... params) {
        ReserveGeneratorWorkerFragment activity = caller.get();
        int reserveCount = params.length;
        ReserveDescriptor[] results = new ReserveDescriptor[reserveCount];

        if (activity != null) {
            ReserveDatabase database = DatabaseFactory.getReserveDatabase(activity.getContext());

            for (int i = 0; i < reserveCount; i++) {
                long reserveId = database.addReserve(params[i]);

                params[i].setReserveID(reserveId);
                results[i] = params[i];

                publishProgress((i / reserveCount) * 100);
            }
        }

        return results;
    }

    @Override
    protected void onPostExecute(ReserveDescriptor[] results) {
        super.onPostExecute(results);

        ReserveGeneratorWorkerFragment parent = caller.get();

        if (parent != null) {
            parent.communicateResult(results[0]);
        }
    }
}
