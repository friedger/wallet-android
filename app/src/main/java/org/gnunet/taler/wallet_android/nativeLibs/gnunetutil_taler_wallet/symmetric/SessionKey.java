/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.symmetric;

import com.ochafik.lang.jnaerator.runtime.NativeSize;
import com.sun.jna.Memory;
import com.sun.jna.Pointer;

import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNET_CRYPTO_SymmetricSessionKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNetLibrary;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUnetVarargLibrary;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.utilities.Codec;
import org.gnunet.taler.wallet_android.utilities.JNAhelpers.MemoryUtils;
import org.gnunet.taler.wallet_android.utilities.JNAhelpers.NativeString;

import java.nio.charset.Charset;

/**
 * Wrapper class for GNUnet session keys
 *
 * @author Oliver Broome
 */
public class SessionKey implements AutoCloseable {
    public static final int KEY_SIZE = 64;
    private GNUNET_CRYPTO_SymmetricSessionKey keyStruct;
    private byte[] saltBytes;
    private boolean isFreeable;

    /**
     * Creates a new random session key.
     */
    public SessionKey() {
        isFreeable = true;
        keyStruct = new GNUNET_CRYPTO_SymmetricSessionKey();
        GNUNetLibrary.GNUNET_CRYPTO_symmetric_create_session_key(keyStruct);

        keyStruct.read();
    }

    /**
     * Wraps an existing session key in memory
     *
     * @param keyStruct the session key Structure object to wrap
     */
    public SessionKey(GNUNET_CRYPTO_SymmetricSessionKey keyStruct) {
        isFreeable = true;
        this.keyStruct = keyStruct;
    }

    /**
     * Derives a key with the given key material. Salt is randomly generated.
     *
     * @param sourceKeyMaterial String to use as source key material
     * @param contextData       context for the HKDF
     */
    public SessionKey(String sourceKeyMaterial, String[] contextData) {
        isFreeable = true;

        Memory keyMemory = new Memory(SessionKey.KEY_SIZE);
        RandomBlock salt = new RandomBlock(RandomBlock.SALT_BLOCK);

        try (NativeString nativeKeyMaterial = new NativeString(sourceKeyMaterial)) {
            runKdf(keyMemory,
                    salt.getPointer(), salt.getNativeSize(),
                    nativeKeyMaterial.getPointer(), nativeKeyMaterial.getNativeSize(),
                    contextData);
        }
    }

    /**
     * Derives a key with the given key material. Salt is randomly generated.
     *
     * @param sourceKeyMaterial Pointer to the source key material in memory
     * @param keyMaterialSize   the size of the source key material
     * @param contextData       context for the HKDF
     */
    public SessionKey(Pointer sourceKeyMaterial, NativeSize keyMaterialSize, String[] contextData) {
        isFreeable = false;

        Memory keyMemory = new Memory(SessionKey.KEY_SIZE);
        RandomBlock salt = new RandomBlock(RandomBlock.SALT_BLOCK);

        saltBytes = salt.getPointer().getByteArray(0, RandomBlock.SALT_BLOCK);

        runKdf(keyMemory,
                salt.getPointer(), salt.getNativeSize(),
                sourceKeyMaterial, keyMaterialSize,
                contextData);
    }

    /**
     * Derives a key with the given key material and salt (currently only necessary for testing)
     *
     * @param keyMaterial     Pointer to the source key material in memory
     * @param keyMaterialSize the size of the source key material
     * @param salt            the custom salt to be used
     * @param contextData     context for the HKDF
     */
    public SessionKey(Pointer keyMaterial, NativeSize keyMaterialSize, CustomSaltBlock salt, String[] contextData) {
        isFreeable = false;

        Memory keyMemory = new Memory(SessionKey.KEY_SIZE);

        saltBytes = salt.getPointer().getByteArray(0, salt.getNativeSize().intValue());

        runKdf(keyMemory, salt.getPointer(), salt.getNativeSize(), keyMaterial, keyMaterialSize, contextData);
    }

    /**
     * Generates the array of pairs of strings and string lengths that the KDFs require for context
     *
     * @param contextData an array of strings that should be used as context
     * @return an array of the inputted strings and their respective lengths.
     */
    protected Object[] addStringSizes(String[] contextData) {
        Object[] result = new Object[contextData.length * 2];

        for (int i = 0; i < contextData.length; i++) {
            result[i] = contextData[i];
            result[i + 1] = new NativeSize(contextData[i].getBytes(Charset.forName("US-ASCII")).length);
        }

        return result;
    }

    /**
     * Runs the actual KDF
     *
     * @param keyMemory         Memory to which the key should be written
     * @param salt              salt to be used
     * @param saltSize          size of the salt in bytes
     * @param sourceKeyMaterial Pointer to the source key material
     * @param keyMaterialSize   size of the memory allocated for the source key material
     * @param contextData       an array of Strings which should be passed to the KDF as context
     */
    private void runKdf(
            Memory keyMemory,
            Pointer salt, NativeSize saltSize,
            Pointer sourceKeyMaterial, NativeSize keyMaterialSize,
            String[] contextData) {
        Object[] varargs = addStringSizes(contextData);

        if (GNUnetVarargLibrary.INSTANCE.GNUNET_CRYPTO_kdf(
                keyMemory,
                new NativeSize(SessionKey.KEY_SIZE),
                salt,
                saltSize,
                sourceKeyMaterial,
                keyMaterialSize,
                varargs
        ) == GNUNetLibrary.GNUNET_YES) {
            keyStruct = new GNUNET_CRYPTO_SymmetricSessionKey(keyMemory);
            keyStruct.read();
        } else {
            throw new RuntimeException("GNUNET_CRYPTO_kdf failed!");
        }
    }

    public GNUNET_CRYPTO_SymmetricSessionKey getData() {
        return keyStruct;
    }

    /**
     * Uses this session key to encrypt arbitrary data from memory
     *
     * @param data     Pointer to the data that should be encrypted
     * @param dataSize size in memory of the passed data
     * @param iv       the InitialisationVector to use
     * @return a new block of Memory with the encrypted data
     */
    public final Memory encrypt(Pointer data, NativeSize dataSize,
                                InitialisationVector iv) {
        Memory encryptedData = new Memory(dataSize.longValue());

        long resultSize = GNUNetLibrary.GNUNET_CRYPTO_symmetric_encrypt(
                data,
                dataSize,
                keyStruct,
                iv.getStruct(),
                encryptedData
        ).longValue();

        if (resultSize == dataSize.longValue()) {
            return encryptedData;
        } else if (resultSize == GNUNetLibrary.GNUNET_SYSERR) {
            throw new RuntimeException("GNUNET_CRYPTO_symmetric_encrypt returned a different block size!");
        } else {
            throw new RuntimeException("GNUNET_CRYPTO_symmetric_encrypt returned an error!");
        }
    }

    public final Memory decrypt(Pointer data, NativeSize size,
                                InitialisationVector iv) {
        Memory decryptedData = new Memory(size.longValue());

        if (GNUNetLibrary.GNUNET_CRYPTO_symmetric_decrypt(
                data,
                size,
                keyStruct,
                iv.getStruct(),
                decryptedData
        ).longValue() != GNUNetLibrary.GNUNET_SYSERR) {
            return decryptedData;
        } else {
            throw new RuntimeException("GNUNET_CRYPTO_symmetric_decrypt returned an error!");
        }
    }

    public final InitialisationVector deriveIV(String[] contextArguments) {
        return new InitialisationVector(this, contextArguments);
    }

    @Override
    public void close() {
        if (keyStruct != null) {
            if (isFreeable) {
                keyStruct.clear();
                MemoryUtils.freeStruct(keyStruct);
            }

            keyStruct = null;
        }
    }

    public String encode() {
        return Codec.encode(keyStruct);
    }

    public byte[] getSalt() {
        return saltBytes;
    }
}
