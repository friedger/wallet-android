/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.database.model.state;

import org.gnunet.taler.wallet_android.database.model.Descriptor;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSAPrivateKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSAPublicKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSASignature;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa.RSABlindingKey;

/**
 * Descriptor object for withdrawals
 *
 * @author Oliver Broome
 */
public class WithdrawalDescriptor implements Descriptor, AutoCloseable {
    private long withdrawalId;
    private long reserveId;
    private EdDSASignature reserveSignature;
    private RSABlindingKey privateBlindKey;
    private EdDSAPrivateKey coinPrivateKey;
    private EdDSAPublicKey coinPublicKey;
    private byte[] coinEnvelope; //(taler.py)c_BlindEnvelope, filled by TalerCrypto.message_blind

    public long getWithdrawalId() {
        return withdrawalId;
    }

    public void setWithdrawalId(long withdrawalId) {
        this.withdrawalId = withdrawalId;
    }

    public long getReserveId() {
        return reserveId;
    }

    public void setReserveId(long reserveId) {
        this.reserveId = reserveId;
    }

    public EdDSASignature getReserveSignature() {
        return reserveSignature;
    }

    public void setReserveSignature(EdDSASignature reserveSignature) {
        this.reserveSignature = reserveSignature;
    }

    public RSABlindingKey getPrivateBlindKey() {
        return privateBlindKey;
    }

    public void setPrivateBlindKey(RSABlindingKey privateBlindKey) {
        this.privateBlindKey = privateBlindKey;
    }

    public byte[] getCoinEnvelope() {
        return coinEnvelope;
    }

    public void setCoinEnvelope(byte[] coinEnvelope) {
        this.coinEnvelope = coinEnvelope;
    }

    public EdDSAPrivateKey getCoinPrivateKey() {
        return coinPrivateKey;
    }

    public EdDSAPublicKey getCoinPublicKey() {
        return coinPublicKey;
    }

    @Override
    public void close() {
        if (reserveSignature != null) {
            reserveSignature.close();
            reserveSignature = null;
        }

        if (privateBlindKey != null) {
            privateBlindKey.close();
            privateBlindKey = null;
        }

        if (coinPrivateKey != null) {
            coinPrivateKey.close();
            coinPrivateKey = null;
        }

        if (coinPublicKey != null) {
            coinPublicKey.close();
            coinPublicKey = null;
        }
    }
}
