/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.database.storage;

/**
 * Constants for SQLite query building, mainly to save you from typing a few more characters.
 *
 * @author Oliver Broome
 */
public interface DBConstants {
    String INTEGER = " INTEGER";
    String TEXT = " TEXT";
    String BLOB = " BLOB";
    String BOOLEAN = " BOOLEAN";
    String PRIMARY_KEY = " PRIMARY KEY ";
    String AUTOINCREMENT = " AUTOINCREMENT ";
    String UNIQUE = " UNIQUE ";
    String NOT_NULL = " NOT NULL";
    String CHECK = " CHECK ";
    String REFERENCES = " REFERENCES ";
    String DEFAULT = " DEFAULT ";
    String FALSE = " FALSE";
    String NEWLINE = "";
    String NEXT_COLUMN = "," + NEWLINE;
    String BEGIN_COLUMNS = " (" + NEWLINE;
    String END_COLUMNS = NEWLINE + ");";
    String SELECT = "SELECT ";
    String FROM = " FROM ";
    String WHERE = " WHERE ";
    String ON_DELETE = " ON DELETE";
    String SET_NULL = " SET NULL";
    String CASCADE = " CASCADE";
    String LIMIT = " LIMIT ";
    String AND = " AND ";
    String EQUALS = " = ";
    String IS_PARAMETER = EQUALS + "? ";
    String DOT = ".";
    String COMMA = ",";
    String AS = " AS ";
    String ASC = " ASC";
}
