/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.symmetric;

import com.ochafik.lang.jnaerator.runtime.NativeSize;
import com.sun.jna.Memory;
import com.sun.jna.Pointer;

import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNET_CRYPTO_SymmetricInitializationVector;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNetLibrary;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUnetVarargLibrary;
import org.gnunet.taler.wallet_android.utilities.JNAhelpers.MemoryUtils;

/**
 * Wrapper class for initialisation vectors used by SessionKey
 *
 * @author Oliver Broome
 */
public class InitialisationVector extends NativeContextUser implements AutoCloseable {
    private GNUNET_CRYPTO_SymmetricInitializationVector initialisationVector;
    private byte[] saltBytes;

    /**
     * Wraps an existing initialisation vector in memory
     *
     * @param ivStruct  the Structure to be wrapped
     * @param saltBytes the salt used to generate this IV
     */
    public InitialisationVector(GNUNET_CRYPTO_SymmetricInitializationVector ivStruct, byte[] saltBytes) {
        initialisationVector = ivStruct;
        this.saltBytes = saltBytes;
    }

    /**
     * Derives an initialisation vector from the given SessionKey
     *
     * @param sessionKey       the SessionKey to derive the IV from
     * @param contextArguments additional context for the derivation function
     */
    public InitialisationVector(SessionKey sessionKey, String[] contextArguments) {
        initialisationVector =
                new GNUNET_CRYPTO_SymmetricInitializationVector();

        RandomBlock salt = new RandomBlock(RandomBlock.SALT_BLOCK);
        saltBytes = salt.getPointer().getByteArray(0, (int) salt.getPointer().size());

        Object[] varargs = generateStringAndSizeVarargs(contextArguments, false);

        GNUnetVarargLibrary.INSTANCE.GNUNET_CRYPTO_symmetric_derive_iv(
                initialisationVector,
                sessionKey.getData(),
                salt.getPointer(),
                salt.getNativeSize(),
                varargs
        );

        initialisationVector.read();
    }

    /**
     * Derives an initialisation vector from the given SessionKey
     *
     * @param sessionKey       the SessionKey to derive the IV from
     * @param salt             the custom salt to use for derivation
     * @param contextArguments additional context for the derivation function
     */
    public InitialisationVector(SessionKey sessionKey, RandomBlock salt, String[] contextArguments) {
        initialisationVector =
                new GNUNET_CRYPTO_SymmetricInitializationVector();

        saltBytes = salt.getPointer().getByteArray(0, (int) salt.getPointer().size());

        Object[] varargs = generateStringAndSizeVarargs(contextArguments, false);

        GNUnetVarargLibrary.INSTANCE.GNUNET_CRYPTO_symmetric_derive_iv(
                initialisationVector,
                sessionKey.getData(),
                salt.getPointer(),
                salt.getNativeSize(),
                varargs
        );

        initialisationVector.read();
    }

    /**
     * Derives a custom IV using the KDF function (hacky solution to the issue with the TestEncryptionEndpoint)
     *
     * @param keyMaterial     Pointer to the key material
     * @param keyMaterialSize size of the key material in memory
     * @param keySize         the size in bytes that the resuling IV should have
     * @param salt            the CustomSaltBlock to use
     * @param contextData     additional context for the KDF
     */
    public InitialisationVector(Pointer keyMaterial, NativeSize keyMaterialSize, NativeSize keySize, RandomBlock salt, String[] contextData) {
        Memory keyMemory = new Memory(keySize.longValue());

        saltBytes = salt.getPointer().getByteArray(0, salt.getNativeSize().intValue());

        Object[] varargs = generateStringAndSizeVarargs(contextData, false);

        if (GNUnetVarargLibrary.INSTANCE.GNUNET_CRYPTO_kdf(
                keyMemory,
                keySize,
                salt.getPointer(),
                salt.getNativeSize(),
                keyMaterial,
                keyMaterialSize,
                varargs
        ) == GNUNetLibrary.GNUNET_YES) {
            initialisationVector = new GNUNET_CRYPTO_SymmetricInitializationVector(keyMemory);
            initialisationVector.read();
        } else {
            throw new RuntimeException("GNUNET_CRYPTO_kdf failed!");
        }
    }

    public GNUNET_CRYPTO_SymmetricInitializationVector getStruct() {
        return initialisationVector;
    }

    public byte[] getSaltBytes() {
        return saltBytes;
    }

    @Override
    public void close() {
        if (initialisationVector != null) {
            initialisationVector.clear();
            MemoryUtils.freeStruct(initialisationVector);
            initialisationVector = null;
        }
    }
}