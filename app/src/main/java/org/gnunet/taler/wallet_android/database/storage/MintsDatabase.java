/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.database.storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.support.annotation.NonNull;
import android.util.Log;

import org.gnunet.taler.wallet_android.database.model.infrastructure.MintDescriptor;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSAPublicKey;

import java.net.MalformedURLException;
import java.net.URL;

import static org.gnunet.taler.wallet_android.database.storage.DBConstants.AUTOINCREMENT;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.BEGIN_COLUMNS;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.BLOB;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.CHECK;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.END_COLUMNS;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.INTEGER;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.NEXT_COLUMN;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.NOT_NULL;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.PRIMARY_KEY;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.TEXT;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.UNIQUE;

/**
 * Allows access to the mint table
 *
 * @author Oliver Broome
 */
public class MintsDatabase extends AbstractDatabase {
    public static final String TABLE_NAME = "mints";

    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_URL = "url";
    public static final String COLUMN_MASTER_PUB = "master_pub";

    public static final String TABLE_CREATE =
            "CREATE TABLE IF NOT EXISTS " + MintsDatabase.TABLE_NAME + BEGIN_COLUMNS +
                    BaseColumns._ID + INTEGER + PRIMARY_KEY + AUTOINCREMENT + NEXT_COLUMN +
                    COLUMN_NAME + TEXT + UNIQUE + NOT_NULL + NEXT_COLUMN +
                    COLUMN_URL + TEXT + UNIQUE + NEXT_COLUMN +
                    COLUMN_MASTER_PUB + BLOB + UNIQUE + CHECK + "(length(" + MintsDatabase.COLUMN_MASTER_PUB + ") = 32)" +
                    END_COLUMNS;

    public static final String ID_WHERE = BaseColumns._ID + " = ?";
    public static final String NAME_WHERE = COLUMN_NAME + " = ?";

    public static final String[] MINT_PROJECTION = new String[]{
            BaseColumns._ID, COLUMN_NAME, COLUMN_URL, COLUMN_MASTER_PUB
    };

    public MintsDatabase(Context context, SQLiteOpenHelper databaseHelper) {
        super(context, databaseHelper);
    }

    /**
     * Adds a new mint to the database
     *
     * @param mintName  the mint's name
     * @param mintUrl   the mint's URL
     * @param publicKey the mint's public EdDSA key
     * @return the row id of the newly inserted mint
     */
    public long addMint(@NonNull String mintName, URL mintUrl, EdDSAPublicKey publicKey) {
        ContentValues content = new ContentValues(3);

        content.put(COLUMN_NAME, mintName);
        content.put(COLUMN_URL, mintUrl.toExternalForm());
        content.put(COLUMN_MASTER_PUB, publicKey.getStruct().q_y);

        try (SQLiteDatabase database = databaseHelper.getWritableDatabase()) {
            return database.insert(TABLE_NAME, null, content);
        }
    }

    /**
     * Deletes a mint from the database
     *
     * @param mintId the row ID of the mint to be deleted
     * @return true, if the deletion succeeded
     */
    public boolean deleteMint(long mintId) {
        try (SQLiteDatabase database = databaseHelper.getWritableDatabase()) {
            return database.delete(TABLE_NAME, ID_WHERE, new String[]{Long.toString(mintId)}) == 1;
        }
    }

    /**
     * Updates an existing mint in the database.
     *
     * @param mintDescriptor the MintDescriptor with the updated information.
     *                       All necessary fields must be set, i.e. row ID, etc.
     * @return true, if the mint was updated successfully
     */
    public boolean updateMint(MintDescriptor mintDescriptor) {
        try (SQLiteDatabase database = databaseHelper.getWritableDatabase()) {
            ContentValues values = new ContentValues();

            values.put(COLUMN_NAME, mintDescriptor.getName());
            values.put(COLUMN_URL, mintDescriptor.getUrl().toExternalForm());
            values.put(COLUMN_MASTER_PUB, mintDescriptor.getPublicKey().getStruct().q_y);

            return database.update(TABLE_NAME,
                    values,
                    ID_WHERE,
                    new String[]{Long.toString(mintDescriptor.getMintID())}) == 1;
        }
    }

    /**
     * Loads all mints from the database.
     *
     * @param limit limits the number of mints returned
     * @return a Cursor pointing to the result set
     */
    public Cursor getAllMints(int limit) {
        String limiter;

        if (limit < 0) {
            limiter = null;
        } else {
            limiter = Integer.toString(limit);
        }

        SQLiteDatabase database = databaseHelper.getReadableDatabase();

        return database.query(
                TABLE_NAME,
                MINT_PROJECTION,
                null, null, null, null, null,
                limiter
        );

    }

    /**
     * Retrieves a single mint with a specific row ID from the database
     *
     * @param id the desired row ID
     * @return a MintDescriptor for the requested mint
     */
    public MintDescriptor getMintById(long id) {
        try (SQLiteDatabase database = databaseHelper.getReadableDatabase()) {
            Reader resultReader = readerFor(
                    database.query(
                            TABLE_NAME,
                            MINT_PROJECTION,
                            ID_WHERE,
                            new String[]{Long.toString(id)},
                            null, null, null));

            resultReader.getNext();
            MintDescriptor result = resultReader.getCurrent();
            resultReader.close();

            return result;
        }
    }

    /**
     * Removes all mints from the database (only for testing use)
     */
    public void clearAllMints() {
        try (SQLiteDatabase database = databaseHelper.getReadableDatabase()) {
            database.delete(TABLE_NAME, null, null);
        }
    }

    public Reader readerFor(Cursor cursor) {
        return new Reader(cursor);
    }

    public class Reader extends DatabaseReader {

        public Reader(Cursor cursor) {
            super(cursor);
        }

        @Override
        public MintDescriptor getCurrent() {
            long id = cursor.getLong(cursor.getColumnIndexOrThrow(BaseColumns._ID));
            String name = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME));
            URL url;

            try {
                url = new URL(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_URL)));
            } catch (MalformedURLException e) {
                Log.w("taler/mint", "Mint entry had a malformed URL!", e);
                throw new RuntimeException("Could not create a Mint object!");
            }

            EdDSAPublicKey publicKey = new EdDSAPublicKey(cursor.getBlob(cursor.getColumnIndexOrThrow(COLUMN_MASTER_PUB)));

            return new MintDescriptor(id, name, url, publicKey);
        }
    }
}
