Taler wallet for Android
========================
This is the Android implementation of the Taler wallet.
Currently not suitable for toasters or other home appliances.

Perquisites for building from scratch:
--------------------------------------
 - Android SDK / NDK
 - Android Studio (optional, but recommended)
 - JNAerator (for the interface code) (not necessary, the generated code is already part of the source release)

Dependencies:
-------------
 - (Oracle?) Java (1.7+)
 - libgcrypt (1.6.3)
 - GNUnet
 - GNU unistring
 - Taler mint ()

Compilation instructions:
-------------------------

From a terminal, run:
 - `cd app/external`
 - `make` (adjust the Makefile to correspond to your desired platform, if necessary (e.g. x86))
 - copy all .so files from app/src/main/jniLibs/artifacts to app/src/main/jniLibs/`<your device platfrom>`/ (e.g. armeabi-v7, x86, etc., will be automated)
 - use gradle or Android Studio to compile the APK in debug mode